<?php
/**
 * Mail Transport
 * Copyright © 2015 MagePal. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Neoxero\Ses\Model;



class Transport extends \Zend_Mail_Transport_Smtp implements \Magento\Framework\Mail\TransportInterface
{
    /**
     * @var \Magento\Framework\Mail\MessageInterface
     */
    protected $_message;


    /**
     * @param \Magento\Framework\Mail\MessageInterface $message
     * @param \MagePal\GmailSmtpApp\Helper\Data $dataHelper
     * @throws \Zend_Mail_Exception
     */
    public function __construct(\Magento\Framework\Mail\MessageInterface $message,  \Neoxero\Ses\Helper\Data $dataHelper )
    {
        if (!$message instanceof \Zend_Mail) {
            throw new \InvalidArgumentException('The message should be an instance of \Zend_Mail');
        }

       if($dataHelper->getConfigEnabled()){
        //set config
        $smtpConf = [
           'auth' => strtolower($dataHelper->getConfigAuth()),
           'ssl' => $dataHelper->getConfigSsl(),
            'username' => $dataHelper->getConfigUsername(),
           'password' => $dataHelper->getConfigPassword()
        ];
        
        
//        $smtpHost = 'email-smtp.eu-west-1.amazonaws.com';
        $smtpHost = $dataHelper->getConfigSmtpHost();
        parent::__construct($smtpHost, $smtpConf);
        $this->_message = $message;
       }  else {
           parent::__construct($smtpHost, $smtpConf);
       }
    }

    /**
     * Send a mail using this transport
     *
     * @return void
     * @throws \Magento\Framework\Exception\MailException
     */
    public function sendMessage()
    {

        try {
            parent::send($this->_message);
        } catch (\Exception $e) {
            throw new \Magento\Framework\Exception\MailException(new \Magento\Framework\Phrase($e->getMessage()), $e);
        }
    }


    /**
     * Get message
     *
     * @return \Magento\Framework\Mail\MessageInterface
     * @since 100.2.0
     */
    public function getMessage(){
        return $this->_message;
    }

}
