<?php
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Neoxero\Homenx\Block;

/**
 * Main contact form block
 */
class Vertical extends Abstracthomenxblock
{
	public function getHomenxItems(){
		$store = $this->getStore();
		$blocksCollection = $this->getModel('Neoxero\Homenx\Model\Homenx')
			->getCollection()
			->addStoreFilter($store)
			->addFieldToFilter('parent_id', $this->getblocksId())
			->addFieldToFilter('status', 1)
			->setOrder('position', 'ASC')
		;
		return $blocksCollection;
	}
}

