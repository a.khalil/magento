<?php
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Neoxero\Homenx\Block\Adminhtml\Helal;

class Block extends \Magento\Framework\View\Element\Template
{
    /**
     * Block constructor
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_controller = 'adminhtml_homenx';
        $this->_blockGroup = 'Neoxero_Homenx';
        $this->_headerText = __('Homenx');
        $this->_addButtonLabel = __('Add Block');
        parent::_construct();
    }

    public function sayMesssage(){
        return "Saying hello from Block";
    }

	/**
     * @return string
     */
    public function getCreateUrl()
    {
        return $this->getUrl('*/*/newparent');
    }
}
