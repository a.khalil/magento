<?php
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * Sitemap grid link column renderer
 *
 */
namespace Neoxero\Homenx\Block\Adminhtml\Grid\Renderer;

class Code extends \Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer
{
    /**
     * Prepare link to display in grid
     *
     * @param \Magento\Framework\DataObject $row
     * @return string
     */
    public function render(\Magento\Framework\DataObject $row)
    {
		$id = $row->getId();
		if($id>1){
			$type = 'Neoxero\Homenx\Block\Horizontal';
			$template = 'horizontal.phtml';
			if($row->getBolckHookType()==2){
				$type = 'Neoxero\Homenx\Block\Vertical';
				$template = 'vertical.phtml';
			}
			
			$html = '{{block class="'.$type.'" homenx_block_id="'.$id.'" template="Neoxero_Homenx::'.$template.'"}}';
			return $html;
		}
		else{
			return '';
		}
    }
}
