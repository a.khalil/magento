<?php
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Neoxero\Homenx\Block\Adminhtml;

class Parents extends \Magento\Backend\Block\Widget\Grid\Container
{
    /**
     * Block constructor
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_controller = 'adminhtml_homenx';
        $this->_blockGroup = 'Neoxero_Homenx';
        $this->_headerText = __('Homenx');
        $this->_addButtonLabel = __('Add Block Holder');
        parent::_construct();
    }

	/**
     * @return string
     */
    public function getCreateUrl()
    {
        return $this->getUrl('*/*/newparent');
    }
}
