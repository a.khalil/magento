<?php
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Neoxero\Homenx\Block\Adminhtml;

class Homenx extends \Magento\Backend\Block\Widget\Grid\Container
{
    /**
     * Block constructor
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_controller = 'adminhtml_homenx';
        $this->_blockGroup = 'Neoxero_Homenx';
        $this->_headerText = __('Homenx');
        $this->_addButtonLabel = __('Add Item');
        parent::_construct();
    }

}
