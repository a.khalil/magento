<?php
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Neoxero\Homenx\Block;

/**
 * Main contact form block
 */
class Homenx extends Abstracthomenxblock
{
	public function getHomenxItems(){
		$store = $this->getStore();
		$itemCollection = $this->getModel('Neoxero\Homenx\Model\Homenx')
			->getCollection()
			->addStoreFilter($store)
			->addFieldToFilter('parent_id', 1)
			->addFieldToFilter('status', 1)
			->setOrder('position', 'ASC')
		;
		return $itemCollection;
	}
}

