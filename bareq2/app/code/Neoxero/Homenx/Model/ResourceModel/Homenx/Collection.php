<?php

/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Neoxero\Homenx\Model\ResourceModel\Homenx;

/**
 * Homenx resource model collection
 *
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection {

    /**
     * Init resource collection
     *
     * @return void
     */
    protected function _construct() {
        $this->_init('Neoxero\Homenx\Model\Homenx', 'Neoxero\Homenx\Model\ResourceModel\Homenx');
    }

    public function addStoreFilter($store, $adminStore = true) {
        $stores = array();

        if ($store instanceof \Magento\Store\Model\Store) {
            $stores[] = (int) $store->getId();
        }

        $stores[] = 0;
        $storeTable = $this->getTable('homenx_store');
        $this->getSelect()->join(
                        array('stores' => $storeTable), 'main_table.homenx_id = stores.homenx_id', array()
                )
                ->where('stores.store_id in (?)', ($adminStore ? $stores : $store));
        return $this;
    }

}
