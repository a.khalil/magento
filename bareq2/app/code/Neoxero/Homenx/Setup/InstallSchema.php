<?php
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Neoxero\Homenx\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;

/**
 * @codeCoverageIgnore
 */
class InstallSchema implements InstallSchemaInterface
{
    /**
     * {@inheritdoc}
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;

        $installer->startSetup();

		/**
         * Create table 'homenx_parent'
         */

		$table = $installer->getConnection()->newTable(
            $installer->getTable('homenx_parent')
        )->addColumn(
            'parent_id',
            Table::TYPE_INTEGER,
            null,
            ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
            'Parent Id'
        )->addColumn(
            'title',
            Table::TYPE_TEXT,
            255,
            [],
            'Title'
        )->addColumn(
            'bolck_hook_type',
            Table::TYPE_SMALLINT,
            null,
            ['nullable' => false, 'default' => 1],
            'Block Hook types'
        )->addColumn(
            'custom_class',
            Table::TYPE_TEXT,
            255,
            [],
            'Custom class'
        )->addColumn(
            'custom_color',
            Table::TYPE_TEXT,
            255,
            [],
            'Custom color'
        )->addColumn(
            'status',
            Table::TYPE_SMALLINT,
            null,
            ['nullable' => false, 'default' => 1],
            'Status'
        );

		$installer->getConnection()->createTable($table);

        /**
         * Create table 'mgs_homenx'
         */
        $table = $installer->getConnection()->newTable(
            $installer->getTable('homenx')
        )->addColumn(
            'homenx_id',
            Table::TYPE_INTEGER,
            null,
            ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
            'Homenx Id'
        )->addColumn(
            'title',
            Table::TYPE_TEXT,
            255,
            [],
            'Title'
        )->addColumn(
            'bolck_hook_type',
            Table::TYPE_SMALLINT,
            null,
            ['nullable' => false, 'default' => 1],
            'Block Hook types'
        )->addColumn(
            'url',
            Table::TYPE_TEXT,
            255,
            [],
            'Item URL'
        )->addColumn(
            'category_id',
            Table::TYPE_INTEGER,
            null,
            ['nullable' => false, 'default' => 0],
            'Category Id'
        )->addColumn(
            'sub_category_ids',
            Table::TYPE_TEXT,
            '2M',
            [],
            'SUB Category Ids'
        )->addColumn(
            'position',
            Table::TYPE_SMALLINT,
            null,
            ['nullable' => false, 'default' => 0],
            'Item Position'
        )->addColumn(
            'columns',
            Table::TYPE_SMALLINT,
            null,
            ['nullable' => false, 'default' => 0],
            'Columns'
        )->addColumn(
            'align_block',
            Table::TYPE_TEXT,
            255,
            [],
            'Align of block item'
        )->addColumn(
            'align_dropdown',
            Table::TYPE_TEXT,
            255,
            [],
            'Align of dropdown'
        )->addColumn(
            'max_level',
            Table::TYPE_SMALLINT,
            null,
            ['nullable' => true],
            'Max level of sub category'
        )->addColumn(
            'dropdown_position',
            Table::TYPE_SMALLINT,
            null,
            ['nullable' => false, 'default' => 1],
            'Dropdown Position'
        )->addColumn(
            'use_thumbnail',
            Table::TYPE_SMALLINT,
            null,
            ['nullable' => false, 'default' => 2],
            'Thumbnail of category'
        )->addColumn(
            'special_class',
            Table::TYPE_TEXT,
            255,
            [],
            'Custom class'
        )->addColumn(
            'static_content',
            Table::TYPE_TEXT,
            '2M',
            [],
            'Static content'
        )->addColumn(
            'top_content',
            Table::TYPE_TEXT,
            '2M',
            [],
            'Top content'
        )->addColumn(
            'bottom_content',
            Table::TYPE_TEXT,
            '2M',
            [],
            'Bottom content'
        )->addColumn(
            'left_content',
            Table::TYPE_TEXT,
            '2M',
            [],
            'Left content'
        )->addColumn(
            'left_col',
            Table::TYPE_SMALLINT,
            null,
            ['nullable' => true],
            'columns of left'
        )->addColumn(
            'right_content',
            Table::TYPE_TEXT,
            '2M',
            [],
            'Right content'
        )->addColumn(
            'right_col',
            Table::TYPE_SMALLINT,
            null,
            ['nullable' => true],
            'columns of right'
        )->addColumn(
            'status',
            Table::TYPE_SMALLINT,
            null,
            ['nullable' => false, 'default' => 1],
            'Status'
        )->addColumn(
            'from_date',
            Table::TYPE_TIMESTAMP,
            null,
            ['nullable' => true],
            'From date'
        )->addColumn(
            'to_date',
            Table::TYPE_TIMESTAMP,
            null,
            ['nullable' => true],
            'To date'
        )->addColumn(
            'parent_id',
            Table::TYPE_INTEGER,
            null,
            ['nullable' => false, 'default' => 0],
            'Parent Id'
        )->addColumn(
            'store',
            Table::TYPE_INTEGER,
            null,
            ['nullable' => false, 'default' => 0],
            'Store Id'
        )->addColumn(
            'html_label',
            Table::TYPE_TEXT,
            255,
            [],
            'Html label'
        )->addColumn(
            'banner',
            Table::TYPE_TEXT,
            255,
            [],
            'Banner Content'
        );

        $installer->getConnection()->createTable($table);

		/**

         * Create table 'mgs_homenx_store'
         */
        $table = $installer->getConnection()->newTable(
            $installer->getTable('homenx_store')
        )->addColumn(
            'homenx_id',
            Table::TYPE_INTEGER,
            null,
            ['nullable' => false, 'primary' => true],
            'Homenx ID'
        )->addColumn(
            'store_id',
            Table::TYPE_INTEGER,
            null,
            ['unsigned' => true, 'nullable' => false, 'primary' => true],
            'Store ID'
        );

        $installer->getConnection()->createTable($table);

        $installer->endSetup();

    }
}
