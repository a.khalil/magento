<?php
/**
 *
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Neoxero\Homenx\Controller\Adminhtml\Homenx;

use Magento\Backend\App\Action;

class Index extends \Neoxero\Homenx\Controller\Adminhtml\Homenx
{
    /**
     * Index action
     *
     * @return void
     */
    public function execute()
    {
        $this->_initAction();
        $this->_view->getPage()->getConfig()->getTitle()->prepend(__('Homenx Elements'));
        $this->_view->renderLayout();
    }
}
