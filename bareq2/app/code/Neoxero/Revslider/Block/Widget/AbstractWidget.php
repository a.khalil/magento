<?php
/**
 * Neoxero
 *
 * NOTICE OF LICENSE
 *
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Neoxero
 * @package     Neoxero_Revslider
 * @copyright   Copyright (c) 2015 Neoxero (http://neoxero.com/)
 * @license     http://neoxero.com/license-agreement.html
 */

namespace Neoxero\Revslider\Block\Widget;

class AbstractWidget extends \Magento\Framework\View\Element\Template implements \Magento\Widget\Block\BlockInterface
{
	/**
	 * @var \Neoxero\Revslider\Helper\Data
	 */
	protected $_revsliderHelper;

	/**
     * @param \Magento\Framework\View\Element\Template\Context $context     
     * @param \Neoxero\Revslider\Helper\Data                       $revsliderHelper 
     * @param array                                            $data        
     */
	public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Neoxero\Revslider\Helper\Data $revsliderHelper,
        array $data = []
        ) {
        $this->_revsliderHelper = $revsliderHelper;
        parent::__construct($context, $data);
    }

    public function getConfig($key, $default = '')
    {
        if($this->hasData($key))
        {
            return $this->getData($key);
        }
        return $default;
    }
}