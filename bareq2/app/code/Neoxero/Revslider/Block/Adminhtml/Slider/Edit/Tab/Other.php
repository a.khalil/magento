<?php

/**
 * Neoxero
 *
 * NOTICE OF LICENSE
 *
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Neoxero
 * @package     Neoxero_Revslider
 */

namespace Neoxero\Revslider\Block\Adminhtml\Slider\Edit\Tab;

use Neoxero\Revslider\Model\Status;

/**
 * Slider Form.
 * @category Neoxero
 * @package  Neoxero_Revslider
 * @module   Revslider
 * @author   Neoxero Developer
 */
class Other extends \Magento\Backend\Block\Widget\Form\Generic implements \Magento\Backend\Block\Widget\Tab\TabInterface
{
    const FIELD_NAME_SUFFIX = 'slider';

    /**
     * @var \Magento\Config\Model\Config\Structure\Element\Dependency\FieldFactory
     */
    protected $_fieldFactory;

    /**
     * [$_revsliderHelper description].
     *
     * @var \Neoxero\Revslider\Helper\Data
     */
    protected $_revsliderHelper;

    /**
     * [__construct description].
     *
     * @param \Magento\Backend\Block\Template\Context                                $context            [description]
     * @param \Neoxero\Revslider\Helper\Data                                    $revsliderHelper [description]
     * @param \Magento\Framework\Registry                                            $registry           [description]
     * @param \Magento\Framework\Data\FormFactory                                    $formFactory        [description]
     * @param \Magento\Store\Model\System\Store                                      $systemStore        [description]
     * @param \Magento\Config\Model\Config\Structure\Element\Dependency\FieldFactory $fieldFactory       [description]
     * @param array                                                                  $data               [description]
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Neoxero\Revslider\Helper\Data $revsliderHelper,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magento\Config\Model\Config\Structure\Element\Dependency\FieldFactory $fieldFactory,
        array $data = []
    ) {
        $this->_revsliderHelper = $revsliderHelper;
        $this->_fieldFactory = $fieldFactory;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    protected function _prepareLayout()
    {
        $this->getLayout()->getBlock('page.title')->setPageTitle($this->getPageTitle());
    }

    /**
     * Prepare form.
     *
     * @return $this
     */
    protected function _prepareForm()
    {
        $slider = $this->getSlider();
        $isElementDisabled = true;
        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create();

        // dependence field map array
        $fieldMaps = [];

        $form->setHtmlIdPrefix('page_');

        $fieldset = $form->addFieldset('parallax_fieldset', ['legend' => __('Parallax Settings')]);
        $fieldset1 = $form->addFieldset('panzoom_fieldset', ['legend' => __('Pan Zoom Settings')]);
        $fieldset2 = $form->addFieldset('further_fieldset', ['legend' => __('Further Options')]);
		
		$fieldset->addField(
            'parallax',
            'select',
            [
                'name' => 'parallax',
                'label' => __('Parallax'),
                'title' => __('Parallax'),
				'options' => Status::getParallaxType(),
				'note' => __('Possible Values: "mouse" , "scroll" - How the Parallax should act. On Mouse Hover movements and Tilts on Mobile Devices, or on Scroll (scroll is disabled on Mobiles !)'),
            ]
        );
		
		$fieldset->addField(
            'parallaxBgFreeze',
            'select',
            [
                'name' => 'parallaxBgFreeze',
                'label' => __('Parallax Bg Freeze'),
                'title' => __('Parallax Bg Freeze'),
				'options' => Status::getAvailableOnOff(),
				'note' => __('Possible Values: "on", "off"  - if it is off, the Main slide images will also move with Parallax Level 1 on Scroll.'),
            ]
        );
		
		for($i=1; $i<=10; $i++) {
			$fieldset->addField(
				'parallaxLevels'. $i,
				'text',
				[
					'name' => 'parallaxLevels'. $i,
					'label' => __('Level Depth '. $i),
					'title' => __('Level Depth '. $i),
					'note' => __('Defines a level that can be used in Slide Editor for this Slider.'),
				]
			);
		}
		
		$fieldset->addField(
            'parallaxDisableOnMobile',
            'select',
            [
                'name' => 'parallaxDisableOnMobile',
                'label' => __('Parallax Disable On Mobile'),
                'title' => __('Parallax Disable On Mobile'),
				'options' => Status::getAvailableOnOff(),
				'note' => __('Possible Values: "on", "off"  - Turn on/ off Parallax Effect on Mobile Devices'),
            ]
        );
		
		$fieldset1->addField(
            'panZoomDisableOnMobile',
            'select',
            [
                'name' => 'panZoomDisableOnMobile',
                'label' => __('Pan Zoom Disable On Mobile'),
                'title' => __('Pan Zoom Disable On Mobile'),
				'options' => Status::getAvailableOnOff(),
				'note' => __('Possible Values: "on", "off"  - Turn on/ off Pan Zoom Effects on Mobile Devices'),
            ]
        );
		
		$fieldset2->addField(
            'simplifyAll',
            'select',
            [
                'name' => 'simplifyAll',
                'label' => __('Simplify All'),
                'title' => __('Simplify All'),
				'options' => Status::getAvailableOnOff(),
				'note' => __('Possible Values: "on", "off"  - Set all Animation on older Browsers like IE8 and iOS4 Safari to Fade, without splitting letters to save performance'),
            ]
        );
		
        $sliderData = $slider->getData();		
		if(isset($sliderData['params'])) {
			$data = json_decode($sliderData['params']);
			$data = (array)$data;
		} else {
			$data = $sliderData;
		}
		
        $form->setValues($data);
        $form->addFieldNameSuffix(self::FIELD_NAME_SUFFIX);
        $this->setForm($form);

        return parent::_prepareForm();
    }

    public function getSlider()
    {
        return $this->_coreRegistry->registry('slider');
    }

    public function getPageTitle()
    {
        return $this->getSlider()->getId() ? __("Edit Slider '%1'", $this->escapeHtml($this->getSlider()->getTitle())) : __('New Slider');
    }

    /**
     * Prepare label for tab.
     *
     * @return string
     */
    public function getTabLabel()
    {
        return __('Other Settings');
    }

    /**
     * Prepare title for tab.
     *
     * @return string
     */
    public function getTabTitle()
    {
        return __('Other Settings');
    }

    /**
     * {@inheritdoc}
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function isHidden()
    {
        return false;
    }
}
