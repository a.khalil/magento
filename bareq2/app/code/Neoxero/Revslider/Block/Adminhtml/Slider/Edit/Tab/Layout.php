<?php

/**
 * Neoxero
 *
 * NOTICE OF LICENSE
 *
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Neoxero
 * @package     Neoxero_Revslider
 */

namespace Neoxero\Revslider\Block\Adminhtml\Slider\Edit\Tab;

use Neoxero\Revslider\Model\Status;

/**
 * Slider Form.
 * @category Neoxero
 * @package  Neoxero_Revslider
 * @module   Revslider
 * @author   Neoxero Developer
 */
class Layout extends \Magento\Backend\Block\Widget\Form\Generic implements \Magento\Backend\Block\Widget\Tab\TabInterface
{
    const FIELD_NAME_SUFFIX = 'slider';

    /**
     * @var \Magento\Config\Model\Config\Structure\Element\Dependency\FieldFactory
     */
    protected $_fieldFactory;

    /**
     * [$_revsliderHelper description].
     *
     * @var \Neoxero\Revslider\Helper\Data
     */
    protected $_revsliderHelper;

    /**
     * [__construct description].
     *
     * @param \Magento\Backend\Block\Template\Context                                $context            [description]
     * @param \Neoxero\Revslider\Helper\Data                                    $revsliderHelper [description]
     * @param \Magento\Framework\Registry                                            $registry           [description]
     * @param \Magento\Framework\Data\FormFactory                                    $formFactory        [description]
     * @param \Magento\Store\Model\System\Store                                      $systemStore        [description]
     * @param \Magento\Config\Model\Config\Structure\Element\Dependency\FieldFactory $fieldFactory       [description]
     * @param array                                                                  $data               [description]
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Neoxero\Revslider\Helper\Data $revsliderHelper,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magento\Config\Model\Config\Structure\Element\Dependency\FieldFactory $fieldFactory,
        array $data = []
    ) {
        $this->_revsliderHelper = $revsliderHelper;
        $this->_fieldFactory = $fieldFactory;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    protected function _prepareLayout()
    {
        $this->getLayout()->getBlock('page.title')->setPageTitle($this->getPageTitle());
    }

    /**
     * Prepare form.
     *
     * @return $this
     */
    protected function _prepareForm()
    {
        $slider = $this->getSlider();
        $isElementDisabled = true;
        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create();

        // dependence field map array
        $fieldMaps = [];

        $form->setHtmlIdPrefix('page_');

        $fieldset = $form->addFieldset('base_fieldset', ['legend' => __('Layout Styles')]);
		
		$fieldset->addField(
            'spinner',
            'select',
            [
                'name' => 'spinner',
                'label' => __('Spinner'),
                'title' => __('Spinner'),
				'options' => Status::getSpinner(),
				'note' => __('Possible Values: "spinner1" , "spinner2", "spinner3" , "spinner4", "spinner5" - The Layout of Loader. If not defined, it will use the basic spinner.'),
            ]
        );
		
		$fieldset->addField(
            'hideTimerBar',
            'select',
            [
                'name' => 'hideTimerBar',
                'label' => __('Hide Timer Bar'),
                'title' => __('Hide Timer Bar'),
				'options' => Status::getAvailableOnOff(),
				'note' => __('Possible Values: "on" , "off" - it will hide or show the banner timer'),
            ]
        );
		
		$fieldset->addField(
            'fullWidth',
            'select',
            [
                'name' => 'fullWidth',
                'label' => __('Full Width'),
                'title' => __('Full Width'),
				'options' => Status::getAvailableOnOff(),
				'note' => __('Possible Values: "on", "off" - defines if the fullwidth/autoresponsive mode is activated'),
            ]
        );
		
		$fieldset->addField(
            'autoHeight',
            'select',
            [
                'name' => 'autoHeight',
                'label' => __('Auto Height'),
                'title' => __('Auto Height'),
				'options' => Status::getAvailableOnOff(),
				'note' => __('Possible Values: "on", "off" - defines if in fullwidth mode the height of the Slider proportional always can grow. If it is set to "off" the max height is == startheight'),
            ]
        );
		
		$fieldset->addField(
            'minHeight',
            'text',
            [
                'name' => 'minHeight',
                'label' => __('Min Height'),
                'title' => __('Min Height'),
				'note' => __('Possible Values: 0 - 9999 - defines the min height of the Slider. The Slider container height will never be smaller than this value. The Content container is still shrinks linear to the browser width and original width of COntainer, and will be centered vertically in the Slider'),
            ]
        );
		
		$fieldset->addField(
            'fullScreenAlignForce',
            'select',
            [
                'name' => 'fullScreenAlignForce',
                'label' => __('Full Screen Align Force'),
                'title' => __('Full Screen Align Force'),
				'options' => Status::getAvailableOnOff(),
				'note' => __('Possible Values: "on", "off" - Allowed only in FullScreen Mode. It lets the Caption Grid to be the full screen, means all position should happen with aligns and offsets. This allow you to use the faresst corner of the slider to present any caption there.'),
            ]
        );
		
		$fieldset->addField(
            'forceFullWidth',
            'select',
            [
                'name' => 'forceFullWidth',
                'label' => __('Force Full Width'),
                'title' => __('Force Full Width'),
				'options' => Status::getAvailableOnOff(),
				'note' => __('Possible Values: "on", "off" - Force the FullWidth Size even if the slider embeded in a boxed container. It can provide higher Performance usage on CPU. Try first set it to "off" and use fullwidth container instead of using this option.'),
            ]
        );
		
		$fieldset->addField(
            'fullScreen',
            'select',
            [
                'name' => 'fullScreen',
                'label' => __('Full Screen'),
                'title' => __('Full Screen'),
				'options' => Status::getAvailableOnOff(),
				'note' => __('Possible Values: "on", "off" - defines if the fullscreen mode is activated'),
            ]
        );
		
		$fieldset->addField(
            'fullScreenOffsetContainer',
            'text',
            [
                'name' => 'fullScreenOffsetContainer',
                'label' => __('Full Screen Offset Container'),
                'title' => __('Full Screen Offset Container'),
				'note' => __('The value is a Container ID or Class i.e. "#topheader" - The Height of Fullheight will be increased with this Container height !'),
            ]
        );
		
		$fieldset->addField(
            'fullScreenOffset',
            'text',
            [
                'name' => 'fullScreenOffset',
                'label' => __('Full Screen Offset'),
                'title' => __('Full Screen Offset'),
				'note' => __('A px or % value which will be added/reduced of the Full Height Container calculation.'),
            ]
        );

        $fieldset->addField(
            'shadow',
            'text',
            [
                'name' => 'shadow',
                'label' => __('Shadow'),
                'title' => __('Shadow'),
				'note' => __('Possible values: 0,1,2,3 (0 == no Shadow, 1,2,3 - Different Shadow Types)'),
            ]
        );
		
		$fieldset->addField(
            'dottedOverlay',
            'select',
            [
                'name' => 'dottedOverlay',
                'label' => __('Dotted Overlay'),
                'title' => __('Dotted Overlay'),
				'options' => Status::getDottedOverlay(),
				'note' => __('Possible Values: "none", "twoxtwo", "threexthree", "twoxtwowhite", "threexthreewhite" - Creates a Dotted Overlay for the Background images extra. Best use for FullScreen / fullwidth sliders, where images are too pixaleted.'),
            ]
        );

        $sliderData = $slider->getData();		
		if(isset($sliderData['params'])) {
			$data = json_decode($sliderData['params']);
			$data = (array)$data;
		} else {
			$data = $sliderData;
		}
		
        $form->setValues($data);
        $form->addFieldNameSuffix(self::FIELD_NAME_SUFFIX);
        $this->setForm($form);

        return parent::_prepareForm();
    }

    public function getSlider()
    {
        return $this->_coreRegistry->registry('slider');
    }

    public function getPageTitle()
    {
        return $this->getSlider()->getId() ? __("Edit Slider '%1'", $this->escapeHtml($this->getSlider()->getTitle())) : __('New Slider');
    }

    /**
     * Prepare label for tab.
     *
     * @return string
     */
    public function getTabLabel()
    {
        return __('Layout Styles');
    }

    /**
     * Prepare title for tab.
     *
     * @return string
     */
    public function getTabTitle()
    {
        return __('Layout Styles');
    }

    /**
     * {@inheritdoc}
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function isHidden()
    {
        return false;
    }
}
