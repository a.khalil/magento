<?php

/**
 * Neoxero
 *
 * NOTICE OF LICENSE
 *
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Neoxero
 * @package     Neoxero_Revslider
 */

namespace Neoxero\Revslider\Block\Adminhtml\Slider\Edit\Tab;

use Neoxero\Revslider\Model\Status;

/**
 * Slider Form.
 * @category Neoxero
 * @package  Neoxero_Revslider
 * @module   Revslider
 * @author   Neoxero Developer
 */
class Navigation extends \Magento\Backend\Block\Widget\Form\Generic implements \Magento\Backend\Block\Widget\Tab\TabInterface
{
    const FIELD_NAME_SUFFIX = 'slider';

    /**
     * @var \Magento\Config\Model\Config\Structure\Element\Dependency\FieldFactory
     */
    protected $_fieldFactory;

    /**
     * [$_revsliderHelper description].
     *
     * @var \Neoxero\Revslider\Helper\Data
     */
    protected $_revsliderHelper;

    /**
     * [__construct description].
     *
     * @param \Magento\Backend\Block\Template\Context                                $context            [description]
     * @param \Neoxero\Revslider\Helper\Data                                    $revsliderHelper [description]
     * @param \Magento\Framework\Registry                                            $registry           [description]
     * @param \Magento\Framework\Data\FormFactory                                    $formFactory        [description]
     * @param \Magento\Store\Model\System\Store                                      $systemStore        [description]
     * @param \Magento\Config\Model\Config\Structure\Element\Dependency\FieldFactory $fieldFactory       [description]
     * @param array                                                                  $data               [description]
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Neoxero\Revslider\Helper\Data $revsliderHelper,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magento\Config\Model\Config\Structure\Element\Dependency\FieldFactory $fieldFactory,
        array $data = []
    ) {
        $this->_revsliderHelper = $revsliderHelper;
        $this->_fieldFactory = $fieldFactory;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    protected function _prepareLayout()
    {
        $this->getLayout()->getBlock('page.title')->setPageTitle($this->getPageTitle());
    }

    /**
     * Prepare form.
     *
     * @return $this
     */
    protected function _prepareForm()
    {
        $slider = $this->getSlider();
        $isElementDisabled = true;
        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create();

        // dependence field map array
        $fieldMaps = [];

        $form->setHtmlIdPrefix('page_');

        $fieldset = $form->addFieldset('base_fieldset', ['legend' => __('Navigation Settings')]);

        $fieldset->addField(
            'keyboardNavigation',
            'select',
            [
                'name' => 'keyboardNavigation',
                'label' => __('Keyboard Navigation'),
                'title' => __('Keyboard Navigation'),
				'options' => Status::getAvailableOnOff(),
				'note' => __('Possible Values: "on", "off" - Allows to use the Left/Right Arrow for Keyboard navigation when Slider is in Focus.'),
            ]
        );

        $fieldset->addField(
            'onHoverStop',
            'select',
            [
                'name' => 'onHoverStop',
                'label' => __('On Hover Stop'),
                'title' => __('On Hover Stop'),
				'options' => Status::getAvailableOnOff(),
				'note' => __('Possible Values: "on", "off" - Stop the Timer if mouse is hovering the Slider.  Caption animations are not stopped !! They will just play to the end.'),
            ]
        );
		
		$fieldset->addField(
            'thumbWidth',
            'text',
            [
                'name' => 'thumbWidth',
                'label' => __('Thumb Width'),
                'title' => __('Thumb Width'),
				'note' => __('The width of the thumbs in pixel. Thumbs are not responsive from basic. For Responsive Thumbs you will need to create media qury based thumb sizes.'),
            ]
        );
		
		$fieldset->addField(
            'thumbHeight',
            'text',
            [
                'name' => 'thumbHeight',
                'label' => __('Thumb Height'),
                'title' => __('Thumb Height'),
				'note' => __('The height of the thumbs in pixel. Thumbs are not responsive from basic. For Responsive Thumbs you will need to create media qury based thumb sizes.'),
            ]
        );
		
		$fieldset->addField(
            'thumbAmount',
            'text',
            [
                'name' => 'thumbAmount',
                'label' => __('Thumb Amount'),
                'title' => __('Thumb Amount'),
				'note' => __('The Amount of visible Thumbs in the same time.  The rest of the thumbs are only visible on hover, or at slide change.'),
            ]
        );
		
		$fieldset->addField(
            'hideThumbs',
            'text',
            [
                'name' => 'hideThumbs',
                'label' => __('Hide Thumbs'),
                'title' => __('Hide Thumbs'),
				'note' => __('0 - Never hide Thumbs.  1000- 100000 (ms) hide thumbs and navigation arrows, bullets after the predefined ms  (1000ms == 1 sec,  1500 == 1,5 sec etc..)'),
            ]
        );
		
		$fieldset->addField(
            'navigationType',
            'select',
            [
                'name' => 'navigationType',
                'label' => __('Navigation Type'),
                'title' => __('Navigation Type'),
				'options' => Status::getNavigationType(),
				'note' => __('Display type of the "bullet/thumbnail" bar (Default:"none"). <br> Possible values are: "bullet", "thumb", "none"'),
            ]
        );
		
		$fieldset->addField(
            'navigationArrows',
            'select',
            [
                'name' => 'navigationArrows',
                'label' => __('Navigation Arrows'),
                'title' => __('Navigation Arrows'),
				'options' => Status::getNavigationArrows(),
				'note' => __('Display position of the Navigation Arrows (Default: "nexttobullets"). <br> 
				Possible values are "nexttobullets", "solo"<br>
				nexttobullets - arrows added next to the bullets left and right <br>
				solo - arrows can be independent positioned, see further options'),
            ]
        );
		
		$fieldset->addField(
            'navigationStyle',
            'select',
            [
                'name' => 'navigationStyle',
                'label' => __('Navigation Style'),
                'title' => __('Navigation Style'),
				'options' => Status::getNavigationStyle(),
				'note' => __('Look of the navigation bullets if navigationType "bullet" selected. <br>
				Possible values: "preview1", "preview2", "preview3", "preview4", "round", "square", "round-old", "square-old", "navbar-old"'),
            ]
        );
		
		$fieldset->addField(
            'navigationHAlign',
            'select',
            [
                'name' => 'navigationHAlign',
                'label' => __('Navigation Horizontal Align'),
                'title' => __('Navigation Horizontal Align'),
				'options' => Status::getNavigationHAlign(),
				'note' => __('Horizontal Align of the Navigation bullets / thumbs (depending on which Style has been selected). Possible values navigationHAlign: "left","center","right" and naigationVAlign: "top","center","bottom"'),
            ]
        );
		
		$fieldset->addField(
            'navigationVAlign',
            'select',
            [
                'name' => 'navigationVAlign',
                'label' => __('Navigation Vertical Align'),
                'title' => __('Navigation Vertical Align'),
				'options' => Status::getNavigationVAlign(),
				'note' => __('Vertical Align of the Navigation bullets / thumbs (depending on which Style has been selected). Possible values navigationHAlign: "left","center","right" and naigationVAlign: "top","center","bottom"'),
            ]
        );
		
		$fieldset->addField(
            'navigationHOffset',
            'text',
            [
                'name' => 'navigationHOffset',
                'label' => __('Navigation Horizontal Offset'),
                'title' => __('Navigation Horizontal Offset'),
				'note' => __('The Horizontal Offset position of the navigation depending on the aligned position. from -1000 to +1000 any value in px. i.e. -30'),
            ]
        );
		
		$fieldset->addField(
            'navigationVOffset',
            'text',
            [
                'name' => 'navigationVOffset',
                'label' => __('Navigation Vertical Offset'),
                'title' => __('Navigation Vertical Offset'),
				'note' => __('The Vertical Offset position of the navigation depending on the aligned position. from -1000 to +1000 any value in px. i.e. -30'),
            ]
        );
		
		$fieldset->addField(
            'soloArrowLeftHalign',
            'select',
            [
                'name' => 'soloArrowLeftHalign',
                'label' => __('Solo Arrow Left Horizontal Align'),
                'title' => __('Solo Arrow Left Horizontal Align'),
				'options' => Status::getNavigationHAlign(),
				'note' => __('Vertical and Horizontal Align of the Navigation Arrows (left and right independent!) Possible values navigationHAlign: "left","center","right"  and naigationVAlign: "top","center","bottom"'),
            ]
        );
		
		$fieldset->addField(
            'soloArrowRightHalign',
            'select',
            [
                'name' => 'soloArrowRightHalign',
                'label' => __('Solo Arrow Right Horizontal Align'),
                'title' => __('Solo Arrow Right Horizontal Align'),
				'options' => Status::getNavigationHAlign(),
				'note' => __('Vertical and Horizontal Align of the Navigation Arrows (left and right independent!) Possible values navigationHAlign: "left","center","right"  and naigationVAlign: "top","center","bottom"'),
            ]
        );
		
		$fieldset->addField(
            'soloArrowLeftValign',
            'select',
            [
                'name' => 'soloArrowLeftValign',
                'label' => __('Solo Arrow Left Vertical Align'),
                'title' => __('Solo Arrow Left Vertical Align'),
				'options' => Status::getNavigationVAlign(),
				'note' => __('Vertical and Horizontal Align of the Navigation Arrows (left and right independent!) Possible values navigationHAlign: "left","center","right"  and naigationVAlign: "top","center","bottom"'),
            ]
        );
		
		$fieldset->addField(
            'soloArrowRightValign',
            'select',
            [
                'name' => 'soloArrowRightValign',
                'label' => __('Solo Arrow Right Vertical Align'),
                'title' => __('Solo Arrow Right Vertical Align'),
				'options' => Status::getNavigationVAlign(),
				'note' => __('Vertical and Horizontal Align of the Navigation Arrows (left and right independent!) Possible values navigationHAlign: "left","center","right"  and naigationVAlign: "top","center","bottom"'),
            ]
        );
		
		$fieldset->addField(
            'soloArrowLeftHOffset',
            'text',
            [
                'name' => 'soloArrowLeftHOffset',
                'label' => __('Solo Arrow Left Horizontal Offset'),
                'title' => __('Solo Arrow Left Horizontal Offset'),
				'note' => __('The Offset position of the navigation depending on the aligned position.  from -1000 to +1000 any value in px.   i.e. -30   Each Arrow independent'),
            ]
        );
		
		$fieldset->addField(
            'soloArrowRightHOffset',
            'text',
            [
                'name' => 'soloArrowRightHOffset',
                'label' => __('Solo Arrow Right Horizontal Offset'),
                'title' => __('Solo Arrow Right Horizontal Offset'),
				'note' => __('The Offset position of the navigation depending on the aligned position.  from -1000 to +1000 any value in px.   i.e. -30   Each Arrow independent'),
            ]
        );
		
		$fieldset->addField(
            'soloArrowLeftVOffset',
            'text',
            [
                'name' => 'soloArrowLeftVOffset',
                'label' => __('Solo Arrow Left Vertical Offset'),
                'title' => __('Solo Arrow Left Vertical Offset'),
				'note' => __('The Offset position of the navigation depending on the aligned position.  from -1000 to +1000 any value in px.   i.e. -30   Each Arrow independent'),
            ]
        );
		
		$fieldset->addField(
            'soloArrowRightVOffset',
            'text',
            [
                'name' => 'soloArrowRightVOffset',
                'label' => __('Solo Arrow Right Vertical Offset'),
                'title' => __('Solo Arrow Right Vertical Offset'),
				'note' => __('The Offset position of the navigation depending on the aligned position.  from -1000 to +1000 any value in px.   i.e. -30   Each Arrow independent'),
            ]
        );
		
		$fieldset->addField(
            'touchenabled',
            'select',
            [
                'name' => 'touchenabled',
                'label' => __('Touch Enabled'),
                'title' => __('Touch Enabled'),
				'options' => Status::getAvailableOnOff(),
				'note' => __('Enable Swipe Function on touch devices (Default: "on"). Possible values: "on", "off".'),
            ]
        );
		
		$fieldset->addField(
            'swipe_velocity',
            'text',
            [
                'name' => 'swipe_velocity',
                'label' => __('Swipe Velocity'),
                'title' => __('Swipe Velocity'),
				'note' => __('The Sensibility of Swipe Gesture (lower is more sensible) (Default: 0.7). Possible values: 0 - 1'),
            ]
        );
		
		$fieldset->addField(
            'swipe_max_touches',
            'text',
            [
                'name' => 'swipe_max_touches',
                'label' => __('Swipe Max Touches'),
                'title' => __('Swipe Max Touches'),
				'note' => __('Max Amount of Fingers to touch (Default: 1). Possible values: 1 - 5'),
            ]
        );
		
		$fieldset->addField(
            'swipe_min_touches',
            'text',
            [
                'name' => 'swipe_min_touches',
                'label' => __('Swipe Min Touches'),
                'title' => __('Swipe Min Touches'),
				'note' => __('Min Amount of Fingers to touch (Default: 1). Possible values: 1 - 5'),
            ]
        );
		
		$fieldset->addField(
            'drag_block_vertical',
            'select',
            [
                'name' => 'drag_block_vertical',
                'label' => __('Drag Block Vertical'),
                'title' => __('Drag Block Vertical'),
				'options' => Status::getAvailableTrueFalse(),
				'note' => __('Prevent Vertical Scroll on Drag (Default: false). Possible values: true, false'),
            ]
        );

        $sliderData = $slider->getData();		
		if(isset($sliderData['params'])) {
			$data = json_decode($sliderData['params']);
			$data = (array)$data;
		} else {
			$data = $sliderData;
		}
		
        $form->setValues($data);
        $form->addFieldNameSuffix(self::FIELD_NAME_SUFFIX);
        $this->setForm($form);

        return parent::_prepareForm();
    }

    public function getSlider()
    {
        return $this->_coreRegistry->registry('slider');
    }

    public function getPageTitle()
    {
        return $this->getSlider()->getId() ? __("Edit Slider '%1'", $this->escapeHtml($this->getSlider()->getTitle())) : __('New Slider');
    }

    /**
     * Prepare label for tab.
     *
     * @return string
     */
    public function getTabLabel()
    {
        return __('Navigation Settings');
    }

    /**
     * Prepare title for tab.
     *
     * @return string
     */
    public function getTabTitle()
    {
        return __('Navigation Settings');
    }

    /**
     * {@inheritdoc}
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function isHidden()
    {
        return false;
    }
}
