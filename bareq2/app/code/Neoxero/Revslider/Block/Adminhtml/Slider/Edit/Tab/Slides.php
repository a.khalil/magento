<?php

/**
 * Neoxero
 *
 * NOTICE OF LICENSE
 *
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Neoxero
 * @package     Neoxero_Revslider
 */

namespace Neoxero\Revslider\Block\Adminhtml\Slider\Edit\Tab;

use Neoxero\Revslider\Model\Status;

/**
 * Slides tab.
 * @category Neoxero
 * @package  Neoxero_Revslider
 * @module   Revslider
 * @author   Neoxero Developer
 */
class Slides extends \Magento\Backend\Block\Widget\Grid\Extended
{
    /**
     * slide factory.
     *
     * @var \Neoxero\Revslider\Model\ResourceModel\Slide\CollectionFactory
     */
    protected $_slideCollectionFactory;
	
	/**
     * slider collection factory.
     *
     * @var \Neoxero\Revslider\Model\ResourceModel\Slider\CollectionFactory
     */
    protected $_sliderCollectionFactory;

    /**
     * [__construct description].
     *
     * @param \Magento\Backend\Block\Template\Context                         $context
     * @param \Magento\Backend\Helper\Data                                    $backendHelper
     * @param \Neoxero\Revslider\Model\ResourceModel\Slide\CollectionFactory $slideCollectionFactory
     * @param array                                                           $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Helper\Data $backendHelper,
        \Neoxero\Revslider\Model\ResourceModel\Slide\CollectionFactory $slideCollectionFactory,
        \Neoxero\Revslider\Model\ResourceModel\Slider\CollectionFactory $sliderCollectionFactory,
		array $data = []
    ) {
        $this->_slideCollectionFactory = $slideCollectionFactory;
        $this->_sliderCollectionFactory = $sliderCollectionFactory;
		parent::__construct($context, $backendHelper, $data);
    }

    /**
     * _construct
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('slideGrid');
        $this->setDefaultSort('slide_id');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
        if ($this->getRequest()->getParam('id')) {
            $this->setDefaultFilter(array('in_slide' => 1));
        }
    }

    /**
     * add Column Filter To Collection
     */
    protected function _addColumnFilterToCollection($column)
    {
        if ($column->getId() == 'in_slide') {
            $slideIds = $this->_getSelectedSlides();

            if (empty($slideIds)) {
                $slideIds = 0;
            }
            if ($column->getFilter()->getValue()) {
                $this->getCollection()->addFieldToFilter('slide_id', array('in' => $slideIds));
            } else {
                if ($slideIds) {
                    $this->getCollection()->addFieldToFilter('slide_id', array('nin' => $slideIds));
                }
            }
        } else {
            parent::_addColumnFilterToCollection($column);
        }

        return $this;
    }

    /**
     * prepare collection
     */
    protected function _prepareCollection()
    {
        /** @var \Neoxero\Revslider\Model\ResourceModel\Slide\Collection $collection */
		$sliderId = $this->getRequest()->getParam('slider_id');
        $collection = $this->_slideCollectionFactory->create();
		$collection->addFieldToFilter('main_table.slider_id', $sliderId);
        $collection->setIsLoadSliderTitle(TRUE);

        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    /**
     * @return $this
     */
    protected function _prepareColumns()
    {
        $this->addColumn(
            'in_slide',
            [
                'header_css_class' => 'a-center',
                'type' => 'checkbox',
                'name' => 'in_slide',
                'align' => 'center',
                'index' => 'slide_id',
                'values' => $this->_getSelectedSlides(),
            ]
        );

        $this->addColumn(
            'slide_id',
            [
                'header' => __('Slide ID'),
                'type' => 'number',
                'index' => 'slide_id',
                'header_css_class' => 'col-id',
                'column_css_class' => 'col-id',
            ]
        );
        $this->addColumn(
            'name',
            [
                'header' => __('Name'),
                'index' => 'name',
                'class' => 'xxx',
                'width' => '50px',
            ]
        );
		$this->addColumn(
            'image',
            [
                'header' => __('Image'),
                'filter' => false,
                'class' => 'xxx',
                'width' => '50px',
                'renderer' => 'Neoxero\Revslider\Block\Adminhtml\Slide\Helper\Renderer\Image',
            ]
        );
        $this->addColumn(
            'title',
            [
                'header' => __('Slider'),
                'index' => 'title',
		'type' => 'options',
                'filter_index' => 'main_table.slider_id',
                'options' => $this->getSliderAvailableOption(),
            ]
        );
        $this->addColumn(
            'status',
            [
                'header' => __('Status'),
                'index' => 'status',
                'type' => 'options',
                'filter_index' => 'main_table.status',
                'options' => Status::getAvailableStatuses(),
            ]
        );

        $this->addColumn(
            'edit',
            [
                'header' => __('Edit'),
                'filter' => false,
                'sortable' => false,
                'index' => 'stores',
                'header_css_class' => 'col-action',
                'column_css_class' => 'col-action',
                'renderer' => 'Neoxero\Revslider\Block\Adminhtml\Slider\Edit\Tab\Helper\Renderer\EditSlide',
            ]
        );

        /*$this->addColumn(
            'order_slide_slider',
            [
                'header' => __('Order'),
                'name' => 'order_slide_slider',
                'index' => 'order_slide_slider',
                'class' => 'xxx',
                'width' => '50px',
                'editable' => true,
            ]
        );*/

        return parent::_prepareColumns();
    }

    /**
     * @return string
     */
    public function getGridUrl()
    {
        return $this->getUrl('*/*/slidesgrid', ['_current' => true]);
    }

    /**
     * get row url
     * @param  object $row
     * @return string
     */
    public function getRowUrl($row)
    {
        return '';
    }

    public function getSelectedSliderSlides()
    {
        $sliderId = $this->getRequest()->getParam('slider_id');
        if (!isset($sliderId)) {
            return [];
        }
        $slideCollection = $this->_slideCollectionFactory->create();
        $slideCollection->addFieldToFilter('slider_id', $sliderId);

        $slideIds = [];
        foreach ($slideCollection as $slide) {
            $slideIds[$slide->getId()] = ['order_slide_slider' => $slide->getOrderSlide()];
        }

        return $slideIds;
    }

    protected function _getSelectedSlides()
    {
        $slides = $this->getRequest()->getParam('slide');
        if (!is_array($slides)) {
            $slides = array_keys($this->getSelectedSliderSlides());
        }

        return $slides;
    }
	
	/**
     * get slider vailable option
     *
     * @return array
     */
    public function getSliderAvailableOption()
    {
        $option = [];
        $sliderCollection = $this->_sliderCollectionFactory->create()->addFieldToSelect(['title']);

        foreach ($sliderCollection as $slider) {
            $option[$slider->getId()] = $slider->getTitle();
        }

        return $option;
    }

    /**
     * Prepare label for tab.
     *
     * @return string
     */
    public function getTabLabel()
    {
        return __('Slides');
    }

    /**
     * Prepare title for tab.
     *
     * @return string
     */
    public function getTabTitle()
    {
        return __('Slides');
    }

    /**
     * {@inheritdoc}
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function isHidden()
    {
        return true;
    }
}
