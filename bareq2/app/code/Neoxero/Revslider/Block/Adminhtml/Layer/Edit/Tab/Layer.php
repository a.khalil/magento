<?php

/**
 * Neoxero
 *
 * NOTICE OF LICENSE
 *
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Neoxero
 * @package     Neoxero_Revslider
 */

namespace Neoxero\Revslider\Block\Adminhtml\Layer\Edit\Tab;

use Neoxero\Revslider\Model\Status;

/**
 * Layer Form.
 * @category Neoxero
 * @package  Neoxero_Revslider
 * @module   Revslider
 * @author   Neoxero Developer
 */
class Layer extends \Magento\Backend\Block\Widget\Form\Generic implements \Magento\Backend\Block\Widget\Tab\TabInterface
{
    const FIELD_NAME_SUFFIX = 'layer';

    /**
     * @var \Magento\Config\Model\Config\Structure\Element\Dependency\FieldFactory
     */
    protected $_fieldFactory;

    /**
     * [$_revsliderHelper description].
     *
     * @var \Neoxero\Revslider\Helper\Data
     */
    protected $_revsliderHelper;
	
	/**
     * slider factory.
     *
     * @var \Neoxero\Revslider\Model\SlideFactory
     */
    protected $_slideFactory;

    /**
     * [__construct description].
     *
     * @param \Magento\Backend\Block\Template\Context                                $context            [description]
     * @param \Neoxero\Revslider\Helper\Data                                   			 $revsliderHelper 	 [description]
     * @param \Magento\Framework\Registry                                            $registry           [description]
     * @param \Magento\Framework\Data\FormFactory                                    $formFactory        [description]
     * @param \Magento\Store\Model\System\Store                                      $systemStore        [description]
	 * @param \Neoxero\Revslider\Model\SlideFactory                    					 $slideFactory
     * @param \Magento\Config\Model\Config\Structure\Element\Dependency\FieldFactory $fieldFactory       [description]
     * @param array                                                                  $data               [description]
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Neoxero\Revslider\Helper\Data $revsliderHelper,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
		\Neoxero\Revslider\Model\SlideFactory $slideFactory,
        \Magento\Config\Model\Config\Structure\Element\Dependency\FieldFactory $fieldFactory,
        array $data = []
    ) {
        $this->_revsliderHelper = $revsliderHelper;
        $this->_fieldFactory = $fieldFactory;
		$this->_slideFactory = $slideFactory;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    protected function _prepareLayout()
    {
        $this->getLayout()->getBlock('page.title')->setPageTitle($this->getPageTitle());
    }

    /**
     * Prepare form.
     *
     * @return $this
     */
    protected function _prepareForm()
    {
        $layer = $this->getLayer();
		
		$model = $this->_coreRegistry->registry('layer');

        if ($slideId = $this->getRequest()->getParam('current_slide_id')) {
            $model->setSlideId($slideId);
        }
		
        $isElementDisabled = true;
        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create();

        /*
         * declare dependence
         */
        // dependence block
        $dependenceBlock = $this->getLayout()->createBlock(
            'Magento\Backend\Block\Widget\Form\Element\Dependence'
        );

        // dependence field map array
        $fieldMaps = [];

        $form->setHtmlIdPrefix('page_');

        $fieldset = $form->addFieldset('base_fieldset', ['legend' => __('Layer Information')]);

        if ($layer->getId()) {
            $fieldset->addField('layer_id', 'hidden', ['name' => 'layer_id']);
        }

        $fieldset->addField(
            'name',
            'text',
            [
                'name' => 'name',
                'label' => __('Title'),
                'title' => __('Title'),
                'required' => true,
                'class' => 'required-entry',
            ]
        );

        $fieldMaps['status'] = $fieldset->addField(
            'status',
            'select',
            [
                'label' => __('Status'),
                'title' => __('Status'),
                'name' => 'status',
                'options' => Status::getAvailableStatuses(),
                'disabled' => false,
            ]
        );
		
		$slide = $this->_slideFactory->create()->load($slideId);
        if ($slide->getId()) {
            $fieldMaps['slide_id'] = $fieldset->addField(
                'slide_id',
                'select',
                [
                    'label' => __('Slide'),
                    'name' => 'slide_id',
					'required' => true,
                    'values' => [
                        [
                            'value' => $slide->getId(),
                            'label' => $slide->getName(),
                        ],
                    ],
                ]
            );
        } else {
			if ($layer->getId()) {
				$slide = $this->_slideFactory->create()->load($layer->getSlideId());
				$fieldMaps['slide_id'] = $fieldset->addField(
					'slide_id',
					'select',
					[
						'label' => __('Slide'),
						'name' => 'slide_id',
						'required' => true,
						'values' => [
							[
								'value' => $slide->getId(),
								'label' => $slide->getName(),
							],
						],
						'disabled' => true,
					]
				);
			} else {
				$fieldMaps['slide_id'] = $fieldset->addField(
					'slide_id',
					'select',
					[
						'label' => __('Slide'),
						'name' => 'slide_id',
						'required' => true,
						'values' => $model->getAvailableLayers(),
					]
				);
			}
        }

        $fieldMaps['type'] = $fieldset->addField(
            'type',
            'select',
            [
                'label' => __('Layer Type'),
                'name' => 'type',
				'required' => true,
                'values' => $this->_revsliderHelper->getLayerType(),
            ]
        );

        $fieldMaps['text_style'] = $fieldset->addField(
            'text_style',
            'select',
            [
                'label' => __('Styleing Captions'),
                'name' => 'text_style',
                'values' => $this->_revsliderHelper->getTextStyle(),
				'note' => 'These are Styling classes created in the settings.css  You can add unlimited amount of Styles in your own css file, to style your captions at the top level already',
            ]
        );
		
		$fieldMaps['text'] = $fieldset->addField(
            'text',
            'editor',
            [
                'name' => 'text',
                'label' => __('Layer Text'),
                'title' => __('Layer Text'),
                'wysiwyg' => false,
            ]
        );
		
		$fieldMaps['image'] = $fieldset->addField(
            'image',
            'image',
            [
                'title' => __('Layer Image'),
                'label' => __('Layer Image'),
                'name' => 'image',
                'note' => 'Allow image type: jpg, jpeg, gif, png',
            ]
        );
		
		$fieldMaps['image_alt'] = $fieldset->addField(
            'image_alt',
            'text',
            [
                'label' => __('Image Alt'),
                'name' => 'image_alt',
            ]
        );
		
		$fieldMaps['image_width'] = $fieldset->addField(
            'image_width',
            'text',
            [
                'label' => __('Image Width'),
                'name' => 'image_width',
            ]
        );
		
		$fieldMaps['image_height'] = $fieldset->addField(
            'image_height',
            'text',
            [
                'label' => __('Image Height'),
                'name' => 'image_height',
            ]
        );
		
		$fieldMaps['image_lazyload'] = $fieldset->addField(
            'image_lazyload',
            'image',
            [
                'title' => __('Layer Image Lazyload'),
                'label' => __('Layer Image Lazyload'),
                'name' => 'image_lazyload',
                'note' => 'Allow image type: jpg, jpeg, gif, png',
            ]
        );

        $fieldMaps['video_type'] = $fieldset->addField(
            'video_type',
            'select',
            [
                'name' => 'video_type',
                'label' => __('Media in Slide'),
                'title' => __('Media in Slide'),
                'values' => $this->_revsliderHelper->getVideoType(),
            ]
        );

        $fieldMaps['video_id'] = $fieldset->addField(
            'video_id',
            'text',
            [
                'label' => __('Video ID'),
                'name' => 'video_id',
            ]
        );
		
		$fieldMaps['video_width'] = $fieldset->addField(
            'video_width',
            'text',
            [
                'label' => __('Video Width'),
                'name' => 'video_width',
            ]
        );

        $fieldMaps['video_height'] = $fieldset->addField(
            'video_height',
            'text',
            [
                'label' => __('Video Height'),
                'name' => 'video_height',
            ]
        );
		
		$fieldMaps['html5'] = $fieldset->addField(
            'html5',
            'editor',
            [
                'name' => 'html5',
                'label' => __('HTML5 Video'),
                'title' => __('HTML5 Video'),
                'wysiwyg' => false,
            ]
        );
		
		$fieldMaps['data_autoplay'] = $fieldset->addField(
            'data_autoplay',
            'select',
            [
                'name' => 'data_autoplay',
                'label' => __('Data Autoplay'),
                'title' => __('Data Autoplay'),
                'values' => Status::getAvailableTrueFalse(),
            ]
        );
		
		$fieldMaps['data_nextslideatend'] = $fieldset->addField(
            'data_nextslideatend',
            'select',
            [
                'name' => 'data_nextslideatend',
                'label' => __('Data Next Slide At End'),
                'title' => __('Data Next Slide At End'),
                'values' => Status::getAvailableTrueFalse(),
            ]
        );
		
		$fieldMaps['data_thumbimage'] = $fieldset->addField(
            'data_thumbimage',
            'image',
            [
                'title' => __('Data Thumb Image'),
                'label' => __('Data Thumb Image'),
                'name' => 'data_thumbimage',
                'note' => 'Allow image type: jpg, jpeg, gif, png',
            ]
        );
		
		$fieldMaps['data_volume'] = $fieldset->addField(
            'data_volume',
            'select',
            [
                'name' => 'data_volume',
                'label' => __('Data Volume'),
                'title' => __('Data Volume'),
                'values' => [
                    [
                        'value' => '',
                        'label' => __('None'),
                    ],
                    [
                        'value' => 'mute',
                        'label' => __('Mute'),
                    ],
                ],
            ]
        );
		
		$fieldMaps['data_forcerewind'] = $fieldset->addField(
            'data_forcerewind',
            'select',
            [
                'name' => 'data_forcerewind',
                'label' => __('Data Force Rewind'),
                'title' => __('Data Force Rewind'),
                'values' => Status::getAvailableOnOff(),
            ]
        );
		
		$fieldMaps['data_autoplayonlyfirsttime'] = $fieldset->addField(
            'data_autoplayonlyfirsttime',
            'select',
            [
                'name' => 'data_autoplayonlyfirsttime',
                'label' => __('Data auto play only first time'),
                'title' => __('Data auto play only first time'),
                'values' => Status::getAvailableTrueFalse(),
            ]
        );
		
		$fieldMaps['data_forcecover'] = $fieldset->addField(
            'data_forcecover',
            'text',
            [
                'label' => __('Data force cover'),
                'name' => 'data_forcecover',
            ]
        );
		
		$fieldMaps['data_aspectratio'] = $fieldset->addField(
            'data_aspectratio',
            'text',
            [
                'label' => __('Data Aspect Ratio'),
                'name' => 'data_aspectratio',
            ]
        );

        /*
         * Add field map
         */
        foreach ($fieldMaps as $fieldMap) {
            $dependenceBlock->addFieldMap($fieldMap->getHtmlId(), $fieldMap->getName());
        }

        $mappingFieldDependence = $this->getMappingFieldDependence();

        /*
         * Add field dependence
         */
        foreach ($mappingFieldDependence as $dependence) {
            $negative = isset($dependence['negative']) && $dependence['negative'];
            if (is_array($dependence['fieldName'])) {
                foreach ($dependence['fieldName'] as $fieldName) {
                    $dependenceBlock->addFieldDependence(
                        $fieldMaps[$fieldName]->getName(),
                        $fieldMaps[$dependence['fieldNameFrom']]->getName(),
                        $this->getDependencyField($dependence['refField'], $negative)
                    );
                }
            } else {
                $dependenceBlock->addFieldDependence(
                    $fieldMaps[$dependence['fieldName']]->getName(),
                    $fieldMaps[$dependence['fieldNameFrom']]->getName(),
                    $this->getDependencyField($dependence['refField'], $negative)
                );
            }
        }

        /*
         * add child block dependence
         */
        $this->setChild('form_after', $dependenceBlock);

        $defaultData = [
            'text_style' => 'text',
            'text' => 'Add Text Layer',
        ];

        if (!$layer->getId()) {
            $layer->setStatus($isElementDisabled ? Status::STATUS_ENABLED : Status::STATUS_DISABLED);
            $layer->addData($defaultData);
			$form->setValues($layer->getData());
        } else {
			$layerData = (array)(json_decode($layer->getParams()));
			$layerData['layer_id'] = $layer->getId();
			$form->setValues($layerData);
		}
        
        //$form->addFieldNameSuffix(self::FIELD_NAME_SUFFIX);
        $this->setForm($form);

        return parent::_prepareForm();
    }
    
	/**
     * get dependency field.
     *
     * @return Magento\Config\Model\Config\Structure\Element\Dependency\Field [description]
     */
    public function getDependencyField($refField, $negative = false, $separator = ',', $fieldPrefix = '')
    {
        return $this->_fieldFactory->create(
            ['fieldData' => ['value' => (string)$refField, 'negative' => $negative, 'separator' => $separator], 'fieldPrefix' => $fieldPrefix]
        );
    }

    public function getMappingFieldDependence()
    {
        return [
            [
                'fieldName' => ['text_style', 'text'],
                'fieldNameFrom' => 'type',
                'refField' => '1',
            ],
            [
                'fieldName' => ['image', 'image_alt', 'image_lazyload', 'image_width', 'image_height'],
                'fieldNameFrom' => 'type',
                'refField' => '2',
            ],
            [
                'fieldName' => ['video_type', 'video_id', 'video_width', 'video_height', 'html5', 'data_autoplay', 'data_nextslideatend',
				'data_thumbimage', 'data_volume', 'data_forcerewind', 'data_autoplayonlyfirsttime', 'data_forcecover', 'data_aspectratio'],
                'fieldNameFrom' => 'type',
                'refField' => '3',
            ],
			[
                'fieldName' => ['video_id', 'video_width', 'video_height'],
                'fieldNameFrom' => 'video_type',
                'refField' => 'youtube,vimeo',
            ],
			[
                'fieldName' => ['html5'],
                'fieldNameFrom' => 'video_type',
                'refField' => 'html5',
            ],
        ];
    }

    public function getLayer()
    {
        return $this->_coreRegistry->registry('layer');
    }

    public function getPageTitle()
    {
        return $this->getLayer()->getId() ? __("Edit Layer '%1'", $this->escapeHtml($this->getLayer()->getName())) : __('New Layer');
    }

    /**
     * Prepare label for tab.
     *
     * @return string
     */
    public function getTabLabel()
    {
        return __('Layer Information');
    }

    /**
     * Prepare title for tab.
     *
     * @return string
     */
    public function getTabTitle()
    {
        return __('Layer Information');
    }

    /**
     * {@inheritdoc}
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function isHidden()
    {
        return false;
    }
}
