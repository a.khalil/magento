<?php

/**
 * Neoxero
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Neoxero.com license that is
 * available through the world-wide-web at this URL:
 * http://neoxero.com/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Neoxero
 * @package     Neoxero_Revslider

 */

namespace Neoxero\Revslider\Block\Adminhtml\Layer;

use Neoxero\Revslider\Model\Status;

/**
 * Layer grid.
 * @category Neoxero
 * @package  Neoxero_Revslider
 * @module   Revslider
 * @author   Neoxero Developer
 */
class Grid extends \Magento\Backend\Block\Widget\Grid\Extended
{
    /**
     * Layer collection factory.
     *
     * @var \Neoxero\Revslider\Model\ResourceModel\Layer\CollectionFactory
     */
    protected $_layerCollectionFactory;

    /**
     * slide collection factory.
     *
     * @var \Neoxero\Revslider\Model\ResourceModel\Slider\CollectionFactory
     */
    protected $_slideCollectionFactory;

    /**
     * construct.
     *
     * @param \Magento\Backend\Block\Template\Context                         $context
     * @param \Magento\Backend\Helper\Data                                    $backendHelper
     * @param \Neoxero\Revslider\Model\ResourceModel\Layer\CollectionFactory $slideCollectionFactory
     * @param array                                                           $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Helper\Data $backendHelper,
        \Neoxero\Revslider\Model\ResourceModel\Layer\CollectionFactory $layerCollectionFactory,
        \Neoxero\Revslider\Model\ResourceModel\Slide\CollectionFactory $slideCollectionFactory,
        array $data = []
    ) {
        $this->_layerCollectionFactory = $layerCollectionFactory;
        $this->_slideCollectionFactory = $slideCollectionFactory;

        parent::__construct($context, $backendHelper, $data);
    }

    protected function _construct()
    {
        parent::_construct();
        $this->setId('layerGrid');
        $this->setDefaultSort('layer_id');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
    }

    protected function _prepareCollection()
    {

        /** @var \Neoxero\Revslider\Model\ResourceModel\Layer\Collection $collection */
        $collection = $this->_layerCollectionFactory->create();

        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    /**
     * @return $this
     */
    protected function _prepareColumns()
    {
		$this->addColumn(
            'layer_id',
            [
                'header' => __('Layer ID'),
                'type' => 'number',
                'index' => 'layer_id',
                'header_css_class' => 'col-id',
                'column_css_class' => 'col-id',
            ]
        );
        $this->addColumn(
            'name',
            [
                'header' => __('Name'),
                'index' => 'name',
                'class' => 'xxx',
                'width' => '50px',
            ]
        );
		
        $this->addColumn(
            'type',
            [
                'header' => __('Type'),
                'index'   => 'type',
                'type'    => 'options',
                'options' => $this->getTypeAvailableOption(),
                'class' => 'xxx',
                'width' => '50px',
            ]
        );
		        
        $this->addColumn(
            'title',
            [
                'header' => __('Slide'),
                'index'   => 'slide_id',
                'type'    => 'options',
                'options' => $this->getSlideAvailableOption(),
                'class' => 'xxx',
                'width' => '50px',
            ]
        );

        $this->addColumn(
            'status',
            [
                'header' => __('Status'),
                'index' => 'status',
                'type' => 'options',
                'options' => Status::getAvailableStatuses(),
            ]
        );

        $this->addColumn(
            'edit',
            [
                'header' => __('Edit'),
                'type' => 'action',
                'getter' => 'getId',
                'actions' => [
                    [
                        'caption' => __('Edit'),
                        'url' => ['base' => '*/*/edit'],
                        'field' => 'layer_id',
                    ],
                ],
                'filter' => false,
                'sortable' => false,
                'index' => 'stores',
                'header_css_class' => 'col-action',
                'column_css_class' => 'col-action',
            ]
        );
		

        return parent::_prepareColumns();
    }

    /**
     * get slide vailable option
     *
     * @return array
     */
    public function getSlideAvailableOption()
    {
        $option = [];
        $slideCollection = $this->_slideCollectionFactory->create()->addFieldToSelect(['name']);

        foreach ($slideCollection as $slide) {
            $option[$slide->getId()] = $slide->getName();
        }

        return $option;
    }
	
	/**
     * get type vailable option
     *
     * @return array
     */
    public function getTypeAvailableOption()
    {
        $option = array(
			1 => 'Layer Text',
			2 => 'Layer Image',
			3 => 'Layer Video',
		);

        return $option;
    }

    /**
     * @return $this
     */
    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('entity_id');
        $this->getMassactionBlock()->setFormFieldName('layer');

        $this->getMassactionBlock()->addItem(
            'delete',
            [
                'label' => __('Delete'),
                'url' => $this->getUrl('revslideradmin/*/massDelete'),
                'confirm' => __('Are you sure?'),
            ]
        );

        $statuses = Status::getAvailableStatuses();

        array_unshift($statuses, ['label' => '', 'value' => '']);
        $this->getMassactionBlock()->addItem(
            'status',
            [
                'label' => __('Change status'),
                'url' => $this->getUrl('revslideradmin/*/massStatus', ['_current' => true]),
                'additional' => [
                    'visibility' => [
                        'name' => 'status',
                        'type' => 'select',
                        'class' => 'required-entry',
                        'label' => __('Status'),
                        'values' => $statuses,
                    ],
                ],
            ]
        );

        return $this;
    }

    /**
     * @return string
     */
    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', ['_current' => true]);
    }

    /**
     * get row url
     * @param  object $row
     * @return string
     */
    public function getRowUrl($row)
    {
        return $this->getUrl(
            '*/*/edit',
            ['layer_id' => $row->getId()]
        );
    }
}
