<?php

/**
 * Neoxero
 *
 * NOTICE OF LICENSE
 *
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Neoxero
 * @package     Neoxero_Revslider
 */

namespace Neoxero\Revslider\Block\Adminhtml\Layer\Edit\Tab;

use Neoxero\Revslider\Model\Status;

/**
 * Caption Form.
 * @category Neoxero
 * @package  Neoxero_Revslider
 * @module   Revslider
 * @author   Neoxero Developer
 */
class Animation extends \Magento\Backend\Block\Widget\Form\Generic implements \Magento\Backend\Block\Widget\Tab\TabInterface
{
    const FIELD_NAME_SUFFIX = 'caption';

    /**
     * @var \Magento\Config\Model\Config\Structure\Element\Dependency\FieldFactory
     */
    protected $_fieldFactory;

    /**
     * [$_revsliderHelper description].
     *
     * @var \Neoxero\Revslider\Helper\Data
     */
    protected $_revsliderHelper;

    /**
     * [__construct description].
     *
     * @param \Magento\Backend\Block\Template\Context                                $context            [description]
     * @param \Neoxero\Revslider\Helper\Data                                    $revsliderHelper [description]
     * @param \Magento\Framework\Registry                                            $registry           [description]
     * @param \Magento\Framework\Data\FormFactory                                    $formFactory        [description]
     * @param \Magento\Store\Model\System\Store                                      $systemStore        [description]
     * @param \Magento\Config\Model\Config\Structure\Element\Dependency\FieldFactory $fieldFactory       [description]
     * @param array                                                                  $data               [description]
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Neoxero\Revslider\Helper\Data $revsliderHelper,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magento\Config\Model\Config\Structure\Element\Dependency\FieldFactory $fieldFactory,
        array $data = []
    ) {
        $this->_revsliderHelper = $revsliderHelper;
        $this->_fieldFactory = $fieldFactory;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    protected function _prepareLayout()
    {
        $this->getLayout()->getBlock('page.title')->setPageTitle($this->getPageTitle());
    }

    /**
     * Prepare form.
     *
     * @return $this
     */
    protected function _prepareForm()
    {
        $layer = $this->getLayer();
        $isElementDisabled = true;
        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create();

        /*
         * declare dependence
         */
        // dependence block
        $dependenceBlock = $this->getLayout()->createBlock(
            'Magento\Backend\Block\Widget\Form\Element\Dependence'
        );

        // dependence field map array
        $fieldMaps = [];

        $form->setHtmlIdPrefix('page_');

        $fieldset = $form->addFieldset('base_fieldset', ['legend' => __('Animation - Settings')]);

        $fieldset->addField(
            'data_start',
            'text',
            [
                'name' => 'data_start',
                'label' => __('Data Start'),
                'title' => __('Data Start'),
				'note'  => __('The timepoint in millisecond when/at the Caption should move in to the slide.'),
            ]
        );

        $fieldMaps['incomingclasses'] = $fieldset->addField(
            'incomingclasses',
            'select',
            [
                'label' => __('Incoming Effect'),
                'name' => 'incomingclasses',
                'values' => $this->_revsliderHelper->getIncomingEffect(),
				'note'  => __('How the Caption is Transformed before animation starts. All value will be animated to 0 or 1 to remove all transoformation of the Caption.'),
            ]
        );
		
		$fieldMaps['customin'] = $fieldset->addField(
            'customin',
            'editor',
            [
                'name' => 'customin',
                'label' => __('Custom Incoming Effect'),
                'title' => __('Custom Incoming Effect'),
                'wysiwyg' => false,
				'note' => __('Custom Animation (in and out) Parameters set via data-customin and data-customout within the caption div. Parameter should be closed with ";"                                  rotationX:0;rotationY:0;rotationZ:0 - value between -920 and +920. Rotation Direction set via X,Y,Z, Can be mixed                                 scaleX:1;scaleY:1 - value between 0.00 - 10.00 Scale width and height. 1 == 100%                                 skewX:1;skewY:1 - value between 0.00 - 10.00 Skew inVertical and/or horizontal direction 0 = no skew                                 opacity:1 - value between 0.00 - 1.00 Transparencity                                 transformOrigin:center center - Sets the origin around which all transforms occur. By default, it is in the center of the element ("50% 50%"). You can define the values using the keywords "top", "left", "right", or "bottom" or you can use percentages (bottom right corner would be "100% 100%") or pixels.                                 Values:left top / left center / left bottom / center top / center center / center bottom / right top / right center / right bottom or x% y%                                 transformPerspective:300 - To get your elements to have a true 3D visual perspective applied, you must either set the “perspective” property of the parent element or set the special “transformPerspective” of the element itself (common values range from around 200 to 1000, the lower the number the stronger the perspective distortion).                                 x:0;y:0; - the x / y distance of the element in px. i.e. x:-50px means vertical left 50px '),
            ]
        );

        $fieldMaps['outgoingclasses'] = $fieldset->addField(
            'outgoingclasses',
            'select',
            [
                'label' => __('Outgoing Effect'),
                'name' => 'outgoingclasses',
                'values' => $this->_revsliderHelper->getOutgoingEffect(),
				'note'  => __('How the Caption is Transformed before animation starts. All value will be animated to 0 or 1 to remove all transoformation of the Caption.'),
            ]
        );
		
		$fieldMaps['customout'] = $fieldset->addField(
            'customout',
            'editor',
            [
                'name' => 'customout',
                'label' => __('Custom Outgoing Effect'),
                'title' => __('Custom Outgoing Effect'),
                'wysiwyg' => false,
				'note' => __('Custom Animation (in and out) Parameters set via data-customin and data-customout within the caption div. Parameter should be closed with ";"                                  rotationX:0;rotationY:0;rotationZ:0 - value between -920 and +920. Rotation Direction set via X,Y,Z, Can be mixed                                 scaleX:1;scaleY:1 - value between 0.00 - 10.00 Scale width and height. 1 == 100%                                 skewX:1;skewY:1 - value between 0.00 - 10.00 Skew inVertical and/or horizontal direction 0 = no skew                                 opacity:1 - value between 0.00 - 1.00 Transparencity                                 transformOrigin:center center - Sets the origin around which all transforms occur. By default, it is in the center of the element ("50% 50%"). You can define the values using the keywords "top", "left", "right", or "bottom" or you can use percentages (bottom right corner would be "100% 100%") or pixels.                                 Values:left top / left center / left bottom / center top / center center / center bottom / right top / right center / right bottom or x% y%                                 transformPerspective:300 - To get your elements to have a true 3D visual perspective applied, you must either set the “perspective” property of the parent element or set the special “transformPerspective” of the element itself (common values range from around 200 to 1000, the lower the number the stronger the perspective distortion).                                 x:0;y:0; - the x / y distance of the element in px. i.e. x:-50px means vertical left 50px '),
            ]
        );
		
		$fieldMaps['data_speed'] = $fieldset->addField(
            'data_speed',
            'text',
            [
                'label' => __('Data Speed'),
                'name' => 'data_speed',
				'note' => 'The speed in milliseconds of the transition to move the Caption in the Slide at the defined  timepoint.'
            ]
        );
		
		$fieldMaps['data_endspeed'] = $fieldset->addField(
            'data_endspeed',
            'text',
            [
                'label' => __('Data End Speed'),
                'name' => 'data_endspeed',
				'note' => 'The speed in milliseconds of the transition to move the Caption out from the Slide at the defined  timepoint.'
            ]
        );
		
		$fieldMaps['data_easing'] = $fieldset->addField(
            'data_easing',
            'select',
            [
                'label' => __('Data Easing'),
                'name' => 'data_easing',
                'values' => $this->_revsliderHelper->getDataEasing(),
				'note'  => __('The Easing Art how the caption is moved in to the slide (note! Animation art set via Classes !).'),
            ]
        );
		
		$fieldMaps['data_endeasing'] = $fieldset->addField(
            'data_endeasing',
            'select',
            [
                'label' => __('Data End Easing'),
                'name' => 'data_endeasing',
                'values' => $this->_revsliderHelper->getDataEasing(),
				'note'  => __('The Easing Art how the caption is moved out from the slide (note! Animation art set via Classes !).'),
            ]
        );

        $fieldMaps['data_elementdelay'] = $fieldset->addField(
            'data_elementdelay',
            'text',
            [
                'label' => __('Data Element Delay'),
                'name' => 'data_elementdelay',
				'note' => 'A Value between 0 and 1 like 0.1 to make delays between the Splitted Text Element (start) Animations. Higher the amount of words or chars, you should decrease that number!'
            ]
        );
		
		$fieldMaps['data_endelementdelay'] = $fieldset->addField(
            'data_endelementdelay',
            'text',
            [
                'label' => __('Data Endelement Delay'),
                'name' => 'data_endelementdelay',
				'note' => 'A Value between 0 and 1 like 0.1 to make delays between the Splitted Text Element (end) Animations. Higher the amount of words or chars, you should decrease that number!'
            ]
        );
		
		$fieldMaps['data_splitin'] = $fieldset->addField(
            'data_splitin',
            'select',
            [
                'label' => __('Data Split In'),
                'name' => 'data_splitin',
                'values' => $this->_revsliderHelper->getDataSplit(),
				'note'  => __('Split Text Animation (incoming transition) to "words", "chars" or "lines". This will create amazing Animation Effects on one go, without the needs to create more captions.'),
            ]
        );
		
		$fieldMaps['data_splitout'] = $fieldset->addField(
            'data_splitout',
            'select',
            [
                'label' => __('Data Split Out'),
                'name' => 'data_splitout',
                'values' => $this->_revsliderHelper->getDataSplit(),
				'note'  => __('Split Text Animation (outgoing transition) to "words", "chars" or "lines". Only available if data-end is set !'),
            ]
        );
		
		$fieldMaps['data_end'] = $fieldset->addField(
            'data_end',
            'text',
            [
                'label' => __('Data End'),
                'name' => 'data_end',
				'note' => 'The timepoint in millisecond when/at the Caption should move out from the slide.'
            ]
        );
		
		$fieldMaps['custom_css'] = $fieldset->addField(
            'custom_css',
            'editor',
            [
                'name' => 'custom_css',
                'label' => __('Custom CSS'),
                'title' => __('Custom CSS'),
                'wysiwyg' => false,
				'note' => 'Add custom CSS for this layer.'
            ]
        );

        /*
         * Add field map
         */
        foreach ($fieldMaps as $fieldMap) {
            $dependenceBlock->addFieldMap($fieldMap->getHtmlId(), $fieldMap->getName());
        }

        $mappingFieldDependence = $this->getMappingFieldDependence();

        /*
         * Add field dependence
         */
        foreach ($mappingFieldDependence as $dependence) {
            $negative = isset($dependence['negative']) && $dependence['negative'];
            if (is_array($dependence['fieldName'])) {
                foreach ($dependence['fieldName'] as $fieldName) {
                    $dependenceBlock->addFieldDependence(
                        $fieldMaps[$fieldName]->getName(),
                        $fieldMaps[$dependence['fieldNameFrom']]->getName(),
                        $this->getDependencyField($dependence['refField'], $negative)
                    );
                }
            } else {
                $dependenceBlock->addFieldDependence(
                    $fieldMaps[$dependence['fieldName']]->getName(),
                    $fieldMaps[$dependence['fieldNameFrom']]->getName(),
                    $this->getDependencyField($dependence['refField'], $negative)
                );
            }
        }

        /*
         * add child block dependence
         */
        $this->setChild('form_after', $dependenceBlock);

        $defaultData = [
            'data_start' => 500,
            'data_speed' => 1000,
            'data_endspeed' => 0,
            'data_easing' => 'Back.easeInOut',
        ];

        if (!$layer->getId()) {
            $layer->setStatus($isElementDisabled ? Status::STATUS_ENABLED : Status::STATUS_DISABLED);
            $layer->addData($defaultData);
			$form->setValues($layer->getData());
        } else {
			$captions = (array)(json_decode($layer->getCaptions()));
			$form->setValues($captions);
		}
        
        $form->addFieldNameSuffix(self::FIELD_NAME_SUFFIX);
        $this->setForm($form);

        return parent::_prepareForm();
    }
    /**
     * get dependency field.
     *
     * @return Magento\Config\Model\Config\Structure\Element\Dependency\Field [description]
     */
    public function getDependencyField($refField, $negative = false, $separator = ',', $fieldPrefix = '')
    {
        return $this->_fieldFactory->create(
            ['fieldData' => ['value' => (string)$refField, 'negative' => $negative, 'separator' => $separator], 'fieldPrefix' => $fieldPrefix]
        );
    }

    public function getMappingFieldDependence()
    {
        return [
            [
                'fieldName' => ['customin'],
                'fieldNameFrom' => 'incomingclasses',
                'refField' => 'customin',
            ],
            [
                'fieldName' => ['customout'],
                'fieldNameFrom' => 'outgoingclasses',
                'refField' => 'customout',
            ],
        ];
    }

    public function getLayer()
    {
        return $this->_coreRegistry->registry('layer');
    }

    public function getPageTitle()
    {
        return $this->getLayer()->getId() ? __("Edit Layer '%1'", $this->escapeHtml($this->getLayer()->getName())) : __('New Layer');
    }

    /**
     * Prepare label for tab.
     *
     * @return string
     */
    public function getTabLabel()
    {
        return __('Animation');
    }

    /**
     * Prepare title for tab.
     *
     * @return string
     */
    public function getTabTitle()
    {
        return __('Animation');
    }

    /**
     * {@inheritdoc}
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function isHidden()
    {
        return false;
    }
}
