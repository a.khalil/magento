<?php

/**
 * Neoxero
 *
 * NOTICE OF LICENSE
 *
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Neoxero
 * @package     Neoxero_Revslider
 */

namespace Neoxero\Revslider\Block\Adminhtml\Layer\Edit\Tab;

use Neoxero\Revslider\Model\Status;

/**
 * Caption Form.
 * @category Neoxero
 * @package  Neoxero_Revslider
 * @module   Revslider
 * @author   Neoxero Developer
 */
class Caption extends \Magento\Backend\Block\Widget\Form\Generic implements \Magento\Backend\Block\Widget\Tab\TabInterface
{
    const FIELD_NAME_SUFFIX = 'caption';

    /**
     * @var \Magento\Config\Model\Config\Structure\Element\Dependency\FieldFactory
     */
    protected $_fieldFactory;

    /**
     * [$_revsliderHelper description].
     *
     * @var \Neoxero\Revslider\Helper\Data
     */
    protected $_revsliderHelper;

    /**
     * [__construct description].
     *
     * @param \Magento\Backend\Block\Template\Context                                $context            [description]
     * @param \Neoxero\Revslider\Helper\Data                                    $revsliderHelper [description]
     * @param \Magento\Framework\Registry                                            $registry           [description]
     * @param \Magento\Framework\Data\FormFactory                                    $formFactory        [description]
     * @param \Magento\Store\Model\System\Store                                      $systemStore        [description]
     * @param \Magento\Config\Model\Config\Structure\Element\Dependency\FieldFactory $fieldFactory       [description]
     * @param array                                                                  $data               [description]
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Neoxero\Revslider\Helper\Data $revsliderHelper,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magento\Config\Model\Config\Structure\Element\Dependency\FieldFactory $fieldFactory,
        array $data = []
    ) {
        $this->_revsliderHelper = $revsliderHelper;
        $this->_fieldFactory = $fieldFactory;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    protected function _prepareLayout()
    {
        $this->getLayout()->getBlock('page.title')->setPageTitle($this->getPageTitle());
    }

    /**
     * Prepare form.
     *
     * @return $this
     */
    protected function _prepareForm()
    {
        $layer = $this->getLayer();
        $isElementDisabled = true;
        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create();

        /*
         * declare dependence
         */
        // dependence block
        $dependenceBlock = $this->getLayout()->createBlock(
            'Magento\Backend\Block\Widget\Form\Element\Dependence'
        );

        // dependence field map array
        $fieldMaps = [];

        $form->setHtmlIdPrefix('page_');

        $fieldset = $form->addFieldset('base_fieldset', ['legend' => __('Caption Data - Settings')]);

        $fieldset['id_attr'] = $fieldset->addField(
            'id_attr',
            'text',
            [
                'title' => __('Custom ID'),
                'label' => __('Custom ID'),
                'name' => 'id_attr',
                'note' => 'Adds a unique ID to the li of the Layer like id="layer_id" (add only the id)',
            ]
        );
		
		$fieldset['class_attr'] = $fieldset->addField(
            'class_attr',
            'text',
            [
                'title' => __('Custom Class'),
                'label' => __('Custom Class'),
                'name' => 'class_attr',
                'note' => 'Adds a unique class to the li of the Layer like class="layer_class" (add only the classnames, seperated by space)',
            ]
        );
			
		$fieldset->addField(
            'left',
            'text',
            [
                'name' => 'left',
                'label' => __('Data X'),
                'title' => __('Data X'),
				'note'  => __('Possible Values are "left", "center", "right", or any Value between -2500  and 2500.                                 If left/center/right is set, the caption will be siple aligned to the position.  Any other "number" will simple set the left position in px of tha caption.                                   At "left" the left side of the caption is aligned to the left side of the slider.                                 At "center" the center of caption is aligned to the center of slide.                                   At "right" the caption right side is aligned to the right side of the Slider.'),
            ]
        );
		
		$fieldset->addField(
            'top',
            'text',
            [
                'name' => 'top',
                'label' => __('Data Y'),
                'title' => __('Data Y'),
				'note'  => __('Possible Values are "top", "center", "bottom", or any Value between -2500  and 2500.                                  If top/center/bottom is set, the caption will be siple aligned to the position.  Any other "number" will simple set the top position in px of tha caption.                                  At "top" the top side of the caption is aligned to the top side of the slider.                                 At "center" the center of caption is aligned to the center of slide.                                   At "bottom" the caption bottom side is aligned to the bottom side of the Slider.'),
            ]
        );
		
		$fieldset->addField(
            'link',
            'text',
            [
                'name' => 'link',
                'label' => __('Link'),
                'title' => __('Link'),
				'note'  => __('A link on the whole slide pic'),
            ]
        );
		
		$fieldset->addField(
            'data_hoffset',
            'text',
            [
                'name' => 'data_hoffset',
                'label' => __('Data Hoffset'),
                'title' => __('Data Hoffset'),
				'note'  => __('Only works if data-x set to left/center/right. It will move the Caption with the defined "px" from the aligned position.  i.e.  data-x="center" data-hoffset="-100" will put the caption 100px left from the slide center  horizontaly.  '),
            ]
        );
		
		$fieldset->addField(
            'data_voffset',
            'text',
            [
                'name' => 'data_voffset',
                'label' => __('Data Voffset'),
                'title' => __('Data Voffset'),
				'note'  => __('Only works if data-y set to top/center/bottom. It will move the Caption with the defined "px" from the aligned position.  i.e.  data-x="center" data-hoffset="-100" will put the caption 100px left from the slide center  vertically.  '),
            ]
        );

        $fieldset->addField(
            'data_captionhidden',
            'select',
            [
                'label' => __('Data Caption Hidden'),
                'title' => __('Data Caption Hidden'),
                'name' => 'data_captionhidden',
                'options' => Status::getAvailableOnOff(),
            ]
        );

        /*
         * Add field map
         */
        foreach ($fieldMaps as $fieldMap) {
            $dependenceBlock->addFieldMap($fieldMap->getHtmlId(), $fieldMap->getName());
        }

        $mappingFieldDependence = $this->getMappingFieldDependence();

        /*
         * Add field dependence
         */
        foreach ($mappingFieldDependence as $dependence) {
            $negative = isset($dependence['negative']) && $dependence['negative'];
            if (is_array($dependence['fieldName'])) {
                foreach ($dependence['fieldName'] as $fieldName) {
                    $dependenceBlock->addFieldDependence(
                        $fieldMaps[$fieldName]->getName(),
                        $fieldMaps[$dependence['fieldNameFrom']]->getName(),
                        $this->getDependencyField($dependence['refField'], $negative)
                    );
                }
            } else {
                $dependenceBlock->addFieldDependence(
                    $fieldMaps[$dependence['fieldName']]->getName(),
                    $fieldMaps[$dependence['fieldNameFrom']]->getName(),
                    $this->getDependencyField($dependence['refField'], $negative)
                );
            }
        }

        /*
         * add child block dependence
         */
        $this->setChild('form_after', $dependenceBlock);

        $defaultData = [
            'left' => 0,
            'top' => 0,
        ];

        if (!$layer->getId()) {
            $layer->setStatus($isElementDisabled ? Status::STATUS_ENABLED : Status::STATUS_DISABLED);
            $layer->addData($defaultData);
			$form->setValues($layer->getData());
        } else {
			$captions = (array)(json_decode($layer->getCaptions()));
			$form->setValues($captions);
		}
        
        $form->addFieldNameSuffix(self::FIELD_NAME_SUFFIX);
        $this->setForm($form);

        return parent::_prepareForm();
    }
    /**
     * get dependency field.
     *
     * @return Magento\Config\Model\Config\Structure\Element\Dependency\Field [description]
     */
    public function getDependencyField($refField, $negative = false, $separator = ',', $fieldPrefix = '')
    {
        return $this->_fieldFactory->create(
            ['fieldData' => ['value' => (string)$refField, 'negative' => $negative, 'separator' => $separator], 'fieldPrefix' => $fieldPrefix]
        );
    }

    public function getMappingFieldDependence()
    {
        return [
          
        ];
    }

    public function getLayer()
    {
        return $this->_coreRegistry->registry('layer');
    }

    public function getPageTitle()
    {
        return $this->getLayer()->getId() ? __("Edit Layer '%1'", $this->escapeHtml($this->getLayer()->getName())) : __('New Layer');
    }

    /**
     * Prepare label for tab.
     *
     * @return string
     */
    public function getTabLabel()
    {
        return __('Caption Data');
    }

    /**
     * Prepare title for tab.
     *
     * @return string
     */
    public function getTabTitle()
    {
        return __('Caption Data');
    }

    /**
     * {@inheritdoc}
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function isHidden()
    {
        return false;
    }
}
