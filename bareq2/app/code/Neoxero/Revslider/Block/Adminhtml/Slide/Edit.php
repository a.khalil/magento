<?php

/**
 * Neoxero
 *
 * NOTICE OF LICENSE
 *
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Neoxero
 * @package     Neoxero_Revslider
 */

namespace Neoxero\Revslider\Block\Adminhtml\Slide;

/**
 * Slide block edit form container.
 * @category Neoxero
 * @package  Neoxero_Revslider
 * @module   Revslider
 * @author   Neoxero Developer
 */
class Edit extends \Magento\Backend\Block\Widget\Form\Container
{
    /**
     * _construct
     * @return void
     */
    protected function _construct()
    {
        $this->_objectId = 'slide_id';
        $this->_blockGroup = 'Neoxero_Revslider';
        $this->_controller = 'adminhtml_slide';

        parent::_construct();

        $this->buttonList->update('save', 'label', __('Save Slide'));
        $this->buttonList->update('delete', 'label', __('Delete'));

        if ($this->getRequest()->getParam('current_slider_id')) {
            $this->buttonList->remove('save');
            $this->buttonList->remove('delete');

            $this->buttonList->remove('back');
            $this->buttonList->add(
                'close_window',
                [
                    'label' => __('Close Window'),
                    'onclick' => 'window.close();',
                ],
                10
            );

            $this->buttonList->add(
                'save_and_continue',
                [
                    'label' => __('Save and Continue Edit'),
                    'class' => 'save',
                    'onclick' => 'customsaveAndContinueEdit()',
                ],
                10
            );

            $this->buttonList->add(
                'save_and_close',
                [
                    'label' => __('Save and Close'),
                    'class' => 'save_and_close',
                    'onclick' => 'saveAndCloseWindow()',
                ],
                10
            );

            $this->_formScripts[] = "
				require(['jquery'], function($){
					$(document).ready(function(){
						var input = $('<input class=\"custom-button-submit\" type=\"submit\" hidden=\"true\" />');
						$(edit_form).append(input);

						window.customsaveAndContinueEdit = function (){
							edit_form.action = '".$this->getSaveAndContinueUrl()."';
							$('.custom-button-submit').trigger('click');

				        }

			    		window.saveAndCloseWindow = function (){
			    			edit_form.action = '".$this->getSaveAndCloseWindowUrl()."';
							$('.custom-button-submit').trigger('click');
			            }
					});
				});
			";

            if ($slideId = $this->getRequest()->getParam('slide_id')) {
                $this->_formScripts[] = '
					window.slide_id = '.$slideId.';
				';
            }
        } else {
			if ($this->getRequest()->getParam('slide_id')) {
				$this->buttonList->add(
					'create_layer',
					[
						'label' => __('Create Layer'),
						'class' => 'add',
						'onclick' => 'openLayerPopupWindow(\''.$this->getCreateLayerUrl().'\')',
					],
					1
				);
			}
		
            $this->buttonList->add(
                'save_and_continue',
                [
                    'label' => __('Save and Continue Edit'),
                    'class' => 'save',
                    'data_attribute' => [
                        'mage-init' => [
                            'button' => ['event' => 'saveAndContinueEdit', 'target' => '#edit_form'],
                        ],
                    ],
                ],
                10
            );
        }

        if ($this->getRequest()->getParam('saveandclose')) {
            $this->_formScripts[] = 'window.close();';
        }
		
		/*
         * See more at file magento2root/lib/web/mage/adminhtml/grid.js
         */
        $this->_formScripts[] = "
			require(['jquery'], function($){
				window.openLayerPopupWindow = function (url) {
					var left = ($(document).width()-1000)/2, height= $(document).height();
					var create_layer_popupwindow = window.open(url, '_blank','width=1000,resizable=1,scrollbars=1,toolbar=1,'+'left='+left+',height='+height);
					var windowFocusHandle = function(){
						if (create_layer_popupwindow.closed) {
							if (typeof layerGridJsObject !== 'undefined' && create_layer_popupwindow.layer_id) {
								layerGridJsObject.reloadParams['layer[]'].push(create_layer_popupwindow.layer_id + '');
								$(edit_form.slide_layer).val($(edit_form.slide_layer).val() + '&' + create_layer_popupwindow.layer_id + '=' + Base64.encode('order_layer_slide=0'));
				       			layerGridJsObject.setPage(create_layer_popupwindow.layer_id);
				       		}
				       		$(window).off('focus',windowFocusHandle);
						} else {
							$(create_layer_popupwindow).trigger('focus');
							create_layer_popupwindow.alert('".__('You have to save layer and close this window!')."');
						}
					}
					$(window).focus(windowFocusHandle);
				}
			});
		";
    }

    /**
     * Retrieve the save and continue edit Url.
     *
     * @return string
     */
    protected function getSaveAndContinueUrl()
    {
        return $this->getUrl(
            '*/*/save',
            [
                '_current' => true,
                'back' => 'edit',
                'slide_id' => $this->getRequest()->getParam('slide_id'),
                'current_slider_id' => $this->getRequest()->getParam('current_slider_id'),
            ]
        );
    }

    /**
     * Retrieve the save and continue edit Url.
     *
     * @return string
     */
    protected function getSaveAndCloseWindowUrl()
    {
        return $this->getUrl(
            '*/*/save',
            [
                '_current' => true,
                'back' => 'edit',
                'slide_id' => $this->getRequest()->getParam('slide_id'),
                'current_slider_id' => $this->getRequest()->getParam('current_slider_id'),
                'saveandclose' => 1,
            ]
        );
    }
	
	/**
     * get create slide url.
     *
     * @return string
     */
    public function getCreateLayerUrl()
    {
        return $this->getUrl('*/layer/new', ['current_slide_id' => $this->getRequest()->getParam('slide_id')]);
    }
}
