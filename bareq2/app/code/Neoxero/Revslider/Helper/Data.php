<?php

/**
 * Neoxero
 *
 * NOTICE OF LICENSE
 *
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Neoxero
 * @package     Neoxero_Revslider
 */

namespace Neoxero\Revslider\Helper;

use Neoxero\Revslider\Model\Slider;

/**
 * Helper Data
 * @category Neoxero
 * @package  Neoxero_Revslider
 * @module   Revslider
 * @author   Neoxero Developer
 */
class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * @var \Magento\Backend\Model\UrlInterface
     */
    protected $_backendUrl;

    /**
     * Store manager.
     *
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;


    /**
     * [__construct description].
     *
     * @param \Magento\Framework\App\Helper\Context                      $context              [description]
     * @param \Magento\Directory\Helper\Data                             $directoryData        [description]
     * @param \Magento\Directory\Model\ResourceModel\Country\Collection       $countryCollection    [description]
     * @param \Magento\Directory\Model\ResourceModel\Region\CollectionFactory $regCollectionFactory [description]
     * @param \Magento\Store\Model\StoreManagerInterface                 $storeManager         [description]
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Backend\Model\UrlInterface $backendUrl,
        \Magento\Store\Model\StoreManagerInterface $storeManager
    ) {
        parent::__construct($context);
        $this->_backendUrl = $backendUrl;
        $this->_storeManager = $storeManager;
    }

    /**
     * get Base Url Media.
     *
     * @param string $path   [description]
     * @param bool   $secure [description]
     *
     * @return string [description]
     */
    public function getBaseUrlMedia($path = '', $secure = false)
    {
        return $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA, $secure) . $path;
    }

    /**
     * get Slider Slide Url
     * @return string
     */
    public function getSliderSlideUrl()
    {
        return $this->_backendUrl->getUrl('*/*/slides', ['_current' => true]);
    }
	
	/**
     * get Slide Layer Url
     * @return string
     */
    public function getSlideLayerUrl()
    {
        return $this->_backendUrl->getUrl('*/*/layers', ['_current' => true]);
    }
	
	/**
     * get Slide Preview Url
     * @return string
     */
    public function getSlidePreviewUrl()
    {
		return $this->_backendUrl->getUrl('*/*/preview', ['_current' => true]);
    }

    /**
     * get Backend Url
     * @param  string $route
     * @param  array  $params
     * @return string
     */
    public function getBackendUrl($route = '', $params = ['_current' => true])
    {
        return $this->_backendUrl->getUrl($route, $params);
    }

    /**
     *  getLayerType
     * @return array
     */
    public function getLayerType()
    {
        return [
            [
                'label' => __('--------- Please choose type -------'),
                'value' => '',
            ],
            [
                'label' => __('Layer Text'),
                'value' => 1,
            ],
            [
                'label' => __('Layer Image'),
                'value' => 2,
            ],
			[
                'label' => __('Layer Video'),
                'value' => 3,
            ],
        ];
    }

    /**
     * getTextStyle
     * @return array
     */
    public function getTextStyle()
    {
        return [
            'backcorner' => 'backcorner',
			'backcornertop' => 'backcornertop',
			'big_dark' => 'big_dark',
			'boldwide_small_white' => 'boldwide_small_white',
			'boxshadow' => 'boxshadow',
			'excerpt' => 'excerpt',
			'finewide_large_white' => 'finewide_large_white',
			'finewide_medium_white' => 'finewide_medium_white',
			'finewide_small_white' => 'finewide_small_white',
			'finewide_verysmall_white_mw' => 'finewide_verysmall_white_mw',
			'frontcorner' => 'frontcorner',
			'frontcornertop' => 'frontcornertop',
			'grassfloor' => 'grassfloor',
			'huge_red' => 'huge_red',
			'huge_thin_yellow' => 'huge_thin_yellow',
			'large_bg_black' => 'large_bg_black',
			'large_bold_black' => 'large_bold_black',
			'large_bold_darkblue' => 'large_bold_darkblue',
			'large_bold_grey' => 'large_bold_grey',
			'large_bold_white' => 'large_bold_white',
			'large_bold_white_25' => 'large_bold_white_25',
			'large_bolder_white' => 'large_bolder_white',
			'large_text' => 'large_text',
			'largeblackbg' => 'largeblackbg',
			'largegreenbg' => 'largegreenbg',
			'largepinkbg' => 'largepinkbg',
			'largewhitebg' => 'largewhitebg',
			'lightgrey_divider' => 'lightgrey_divider',
			'medium_bg_asbestos' => 'medium_bg_asbestos',
			'medium_bg_darkblue' => 'medium_bg_darkblue',
			'medium_bg_orange' => 'medium_bg_orange',
			'medium_bg_orange_new1' => 'medium_bg_orange_new1',
			'medium_bg_red' => 'medium_bg_red',
			'medium_bold_grey' => 'medium_bold_grey',
			'medium_bold_orange' => 'medium_bold_orange',
			'medium_bold_red' => 'medium_bold_red',
			'medium_dark' => 'medium_dark',
			'medium_grey' => 'medium_grey',
			'medium_light_black' => 'medium_light_black',
			'medium_light_red' => 'medium_light_red',
			'medium_light_white' => 'medium_light_white',
			'medium_text' => 'medium_text',
			'medium_text_shadow' => 'medium_text_shadow',
			'medium_thin_grey' => 'medium_thin_grey',
			'mediumlarge_light_darkblue' => 'mediumlarge_light_darkblue',
			'mediumlarge_light_white' => 'mediumlarge_light_white',
			'mediumlarge_light_white_center' => 'mediumlarge_light_white_center',
			'mediumwhitebg' => 'mediumwhitebg',
			'middle_yellow' => 'middle_yellow',
			'modern_big_bluebg' => 'modern_big_bluebg',
			'modern_big_redbg' => 'modern_big_redbg',
			'modern_medium_fat' => 'modern_medium_fat',
			'modern_medium_fat_white' => 'modern_medium_fat_white',
			'modern_medium_light' => 'modern_medium_light',
			'modern_small_text_dark' => 'modern_small_text_dark',
			'noshadow' => 'noshadow',
			'roundedimage' => 'roundedimage',
			'small_light_white' => 'small_light_white',
			'small_text' => 'small_text',
			'small_thin_grey' => 'small_thin_grey',
			'smoothcircle' => 'smoothcircle',
			'text' => 'text',
			'thinheadline_dark' => 'thinheadline_dark',
			'thintext_dark' => 'thintext_dark',
			'very_big_black' => 'very_big_black',
			'very_big_white' => 'very_big_white',
			'very_large_text' => 'very_large_text',
			'whitedivider3px' => 'whitedivider3px',
			'whitedivider3px_vertical' => 'whitedivider3px_vertical',
        ];
    }

    /**
     * getVideoType
     * @return array
     */
    public function getVideoType()
    {
        return [
            [
                'label' => __('YouTube Video'),
                'value' => 'youtube',
            ],
            [
                'label' => __('Vimeo Video'),
                'value' => 'vimeo',
            ],
			[
                'label' => __('HTML5 Video'),
                'value' => 'html5',
            ],
        ];
    }
	
	/**
     * get Background Position
     * @return array
     */
    public function getBackgroundPosition()
    {
        return [
            [
                'label' => __('center top'),
                'value' => 'center top',
            ],
            [
                'label' => __('center right'),
                'value' => 'center right',
            ],
            [
                'label' => __('center bottom'),
                'value' => 'center bottom',
            ],
            [
                'label' => __('center center'),
                'value' => 'center center',
            ],
			[
                'label' => __('left top'),
                'value' => 'left top',
            ],
			[
                'label' => __('left center'),
                'value' => 'left center',
            ],
			[
                'label' => __('left bottom'),
                'value' => 'left bottom',
            ],
			[
                'label' => __('right top'),
                'value' => 'right top',
            ],
			[
                'label' => __('right center'),
                'value' => 'right center',
            ],
			[
                'label' => __('right bottom'),
                'value' => 'right bottom',
            ],
			[
                'label' => __('(x%, y%)'),
                'value' => 'percentage',
            ],
        ];
    }
	
	/**
     * get Background Fit
     * @return array
     */
	public function getBackgroundFit()
    {
        return [
			[
                'value' => 'cover',
                'label' => __('cover'),
            ],
            [
                'value' => 'contain',
                'label' => __('contain'),
            ],
            [
                'value' => 'percentage',
                'label' => __('(%, %)'),
            ],
			[
                'value' => 'normal',
                'label' => __('normal'),
            ],
        ];
    }

    /**
     * get Background Repeat
     * @return array
     */
	public function getBackgroundRepeat()
    {
        return [
			[
                'value' => 'no-repeat',
                'label' => __('no-repeat'),
            ],
            [
                'value' => 'repeat',
                'label' => __('repeat'),
            ],
            [
                'value' => 'repeat-x',
                'label' => __('repeat-x'),
            ],
			[
                'value' => 'repeat-y',
                'label' => __('repeat-y'),
            ],
        ];
    }
	
	/**
     * getIncomingEffect
     * @return array
     */
	public function getIncomingEffect()
    {
		return [
			'' => 'None',
			'sft' => 'Short from Top',
			'sfb' => 'Short from Bottom',
			'sfr' => 'Short from Right',
			'sfl' => 'Short from Left',
			'lft' => 'Long from Top',
			'lfb' => 'Long from Bottom',
			'lfr' => 'Long from Right',
			'lfl' => 'Long from Left',
			'skewfromleft' => 'Skew from Left',
			'skewfromright' => 'Skew from Right',
			'skewfromleftshort' => 'Skew Short from Left',
			'skewfromrightshort' => 'Skew Short from Right',
			'fade' => 'fading',
			'randomrotate' => 'Fade in, Rotate from a Random position and Degree',
			'customin' => 'Custom Incoming Animation - see below all data settings'
		];
	}
	
	/**
     * getOutgoingEffect
     * @return array
     */
	public function getOutgoingEffect()
    {
		return [
			'' => 'None',
			'stt' => 'Short to Top',
			'stb' => 'Short to Bottom',
			'str' => 'Short to Right',
			'stl' => 'Short to Left',
			'ltt' => 'Long to Top',
			'ltb' => 'Long to Bottom',
			'ltr' => 'Long to Right',
			'ltl' => 'Long to Left',
			'skewtoleft' => 'Skew to Left',
			'skewtoright' => 'Skew to Right',
			'skewtoleftshort' => 'Skew Short to Left',
			'skewtorightshort' => 'Skew Short to Right',
			'fadeout' => 'fading',
			'randomrotateout' => 'Fade in, Rotate from a Random position and Degree',
			'customout' => 'Custom Outgoing Animation - see below all data settings'
		];
	}
	
	/**
     * getDataEasing
     * @return array
     */
	public function getDataEasing()
    {
		return [
			'' => 'None',
			'Linear.easeNone' => 'Linear.easeNone',
			'Power0.easeIn' => 'Power0.easeIn',
			'Power0.easeInOut' => 'Power0.easeInOut',
			'Power0.easeOut' => 'Power0.easeOut',
			'Power1.easeIn' => 'Power1.easeIn',
			'Power1.easeInOut' => 'Power1.easeInOut',
			'Power1.easeOut' => 'Power1.easeOut',
			'Power2.easeIn' => 'Power2.easeIn',
			'Power2.easeInOut' => 'Power2.easeInOut',
			'Power2.easeOut' => 'Power2.easeOut',
			'Power3.easeIn' => 'Power3.easeIn',
			'Power3.easeInOut' => 'Power3.easeInOut',
			'Power3.easeOut' => 'Power3.easeOut',
			'Power4.easeIn' => 'Power4.easeIn',
			'Power4.easeInOut' => 'Power4.easeInOut',
			'Power4.easeOut' => 'Power4.easeOut',
			'Quad.easeIn' => 'Quad.easeIn',
			'Quad.easeInOut' => 'Quad.easeInOut',
			'Quad.easeOut' => 'Quad.easeOut',
			'Cubic.easeIn' => 'Cubic.easeIn',
			'Cubic.easeInOut' => 'Cubic.easeInOut',
			'Cubic.easeOut' => 'Cubic.easeOut',
			'Quart.easeIn' => 'Quart.easeIn',
			'Quart.easeInOut' => 'Quart.easeInOut',
			'Quart.easeOut' => 'Quart.easeOut',
			'Quint.easeIn' => 'Quint.easeIn',
			'Quint.easeInOut' => 'Quint.easeInOut',
			'Quint.easeOut' => 'Quint.easeOut',
			'Strong.easeIn' => 'Strong.easeIn',
			'Strong.easeInOut' => 'Strong.easeInOut',
			'Strong.easeOut' => 'Strong.easeOut',
			'Back.easeIn' => 'Back.easeIn',
			'Back.easeInOut' => 'Back.easeInOut',
			'Back.easeOut' => 'Back.easeOut',
			'Bounce.easeIn' => 'Bounce.easeIn',
			'Bounce.easeInOut' => 'Bounce.easeInOut',
			'Bounce.easeOut' => 'Bounce.easeOut',
			'Circ.easeIn' => 'Circ.easeIn',
			'Circ.easeInOut' => 'Circ.easeInOut',
			'Circ.easeOut' => 'Circ.easeOut',
			'Elastic.easeIn' => 'Elastic.easeIn',
			'Elastic.easeInOut' => 'Elastic.easeInOut',
			'Elastic.easeOut' => 'Elastic.easeOut',
			'Expo.easeIn' => 'Expo.easeIn',
			'Expo.easeInOut' => 'Expo.easeInOut',
			'Expo.easeOut' => 'Expo.easeOut',
			'Sine.easeIn' => 'Sine.easeIn',
			'Sine.easeInOut' => 'Sine.easeInOut',
			'Sine.easeOut' => 'Sine.easeOut',
			'SlowMo.ease' => 'SlowMo.ease',
			'easeOutBack' => 'easeOutBack',
			'easeInQuad' => 'easeInQuad',
			'easeOutQuad' => 'easeOutQuad',
			'easeInOutQuad' => 'easeInOutQuad',
			'easeInCubic' => 'easeInCubic',
			'easeOutCubic' => 'easeOutCubic',
			'easeInOutCubic' => 'easeInOutCubic',
			'easeInQuart' => 'easeInQuart',
			'easeOutQuart' => 'easeOutQuart',
			'easeInOutQuart' => 'easeInOutQuart',
			'easeInQuint' => 'easeInQuint',
			'easeOutQuint' => 'easeOutQuint',
			'easeInOutQuint' => 'easeInOutQuint',
			'easeInSine' => 'easeInSine',
			'easeOutSine' => 'easeOutSine',
			'easeInOutSine' => 'easeInOutSine',
			'easeInExpo' => 'easeInExpo',
			'easeOutExpo' => 'easeOutExpo',
			'easeInOutExpo' => 'easeInOutExpo',
			'easeInCirc' => 'easeInCirc',
			'easeOutCirc' => 'easeOutCirc',
			'easeInOutCirc' => 'easeInOutCirc',
			'easeInElastic' => 'easeInElastic',
			'easeOutElastic' => 'easeOutElastic',
			'easeInOutElastic' => 'easeInOutElastic',
			'easeInBack' => 'easeInBack',
			'easeOutBack' => 'easeOutBack',
			'easeInOutBack' => 'easeInOutBack',
			'easeInBounce' => 'easeInBounce',
			'easeOutBounce' => 'easeOutBounce',
			'easeInOutBounce' => 'easeInOutBounce'
		];
	}
	
	/**
     * getDataSplit
     * @return array
     */
	public function getDataSplit()
    {
		return [
			'' => '',
			'none' => 'None',
			'words' => 'words',
			'chars' => 'chars',
			'lines' => 'lines',
		];
	}
}
