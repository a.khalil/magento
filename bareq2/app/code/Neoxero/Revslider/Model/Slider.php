<?php

/**
 * Neoxero
 *
 * NOTICE OF LICENSE
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Neoxero
 * @package     Neoxero_Revslider
 */

namespace Neoxero\Revslider\Model;

/**
 * Slider Model
 * @category Neoxero
 * @package  Neoxero_Revslider
 * @module   Revslider
 * @author   Neoxero Developer
 */
class Slider extends \Magento\Framework\Model\AbstractModel
{
    const XML_CONFIG_REVSLIDER = 'revslider/general/enable_frontend';

    /**
     * sort type of slides in a slider.
     */
    const SORT_TYPE_RANDOM = 1;
    const SORT_TYPE_ORDERLY = 2;


    /**
     * slide collection factory.
     *
     * @var \Neoxero\Revslider\Model\ResourceModel\Slide\CollectionFactory
     */
    protected $_slideCollectionFactory;

    /**
     * constructor.
     *
     * @param \Magento\Framework\Model\Context                                $context
     * @param \Magento\Framework\Registry                                     $registry
     * @param \Neoxero\Revslider\Model\ResourceModel\Slide\CollectionFactory $slideCollectionFactory
     * @param \Neoxero\Revslider\Model\ResourceModel\Slider                   $resource
     * @param \Neoxero\Revslider\Model\ResourceModel\Slider\Collection        $resourceCollection
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Neoxero\Revslider\Model\ResourceModel\Slide\CollectionFactory $slideCollectionFactory,
        \Neoxero\Revslider\Model\ResourceModel\Slider $resource,
        \Neoxero\Revslider\Model\ResourceModel\Slider\Collection $resourceCollection
    ) {
        parent::__construct(
            $context,
            $registry,
            $resource,
            $resourceCollection
        );
        $this->_slideCollectionFactory = $slideCollectionFactory;
    }

    /**
     * get slide collection of slider.
     *
     * @return \Neoxero\Revslider\Model\ResourceModel\Slide\Collection
     */
    public function getOwnSlideCollection()
    {
        return $this->_slideCollectionFactory->create()->addFieldToFilter('slider_id', $this->getId());
    }

}
