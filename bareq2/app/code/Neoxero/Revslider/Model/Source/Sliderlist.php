<?php
/**
 * Neoxero
 *
 * NOTICE OF LICENSE
 *
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Neoxero
 * @package     Neoxero_Revslider
 */
namespace Neoxero\Revslider\Model\Source;

class Sliderlist implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * @var \Neoxero\Revslider\Model\Slider
     */
    protected  $_slider;
    
    /**
     * 
     * @param \Neoxero\Revslider\Model\Slider $slider
     */
    public function __construct(
        \Neoxero\Revslider\Model\Slider $slider
        ) {
        $this->_slider = $slider;
    }

    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        $sliders = $this->_slider->getCollection()
        ->addFieldToFilter('status', '1');
		
        $sliderList = array();
        foreach ($sliders as $slider) {
            $sliderList[] = array('label' => $slider->getTitle(),
                'value' => $slider->getId());
        }
        return $sliderList;
    }
}
