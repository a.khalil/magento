<?php

/**
 * Neoxero
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Neoxero.com license that is
 * available through the world-wide-web at this URL:
 * http://neoxero.com/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Neoxero
 * @package     Neoxero_Revslider
 * @copyright   Copyright (c) 2015 Neoxero (http://neoxero.com/)
 * @license     http://neoxero.com/license-agreement.html
 */

namespace Neoxero\Revslider\Model;

/**
 * Status
 * @category Neoxero
 * @package  Neoxero_Revslider
 * @module   Revslider
 * @author   Neoxero Developer
 */
class Status
{
    const STATUS_ENABLED = 1;
    const STATUS_DISABLED = 2;

    /**
     * get available statuses.
     *
     * @return []
     */
    public static function getAvailableStatuses()
    {
        return [
            self::STATUS_ENABLED => __('Enabled')
            , self::STATUS_DISABLED => __('Disabled'),
        ];
    }
	
	/**
     * get available on/off.
     *
     * @return []
     */
    public static function getAvailableOnOff()
    {
        return [
            '' => __('None'),
            'on' => __('On'),
            'off' => __('Off'),
        ];
    }
	
	public static function getNavigationType() {
		return [
            '' => __('None'),
            'bullet' => __('Bullet'),
            'thumb' => __('Thumb'),
        ];
	}
	
	public static function getNavigationArrows() {
		return [
			'' => __('None'),
            'nexttobullets' => __('Next to Bullets'),
            'solo' => __('Solo'),
        ];
	}
	
	public static function getNavigationStyle() {
		return [
			'' => __('None'),
            'preview1' => __('Preview 1'),
            'preview2' => __('Preview 2'),
            'preview3' => __('Preview 3'),
            'preview4' => __('Preview 4'),
            'round' => __('Round'),
            'square' => __('Square'),
            'round-old' => __('Round Old'),
            'square-old' => __('Square Old'),
            'navbar-old' => __('Navbar Old'),
        ];
	}
	
	public static function getNavigationHAlign() {
		return [
			'' => __('None'),
            'left' => __('Left'),
            'center' => __('Center'),
            'right' => __('Right'),
        ];
	}
	
	public static function getNavigationVAlign() {
		return [
			'' => __('None'),
            'top' => __('Top'),
            'center' => __('Center'),
            'bottom' => __('Bottom'),
        ];
	}
	
	public static function getAvailableTrueFalse() {
		return [
			'' => __('None'),
            'true' => __('True'),
            'false' => __('False'),
        ];
	}
	
	public static function getDottedOverlay() {
		return [
            'none' => __('None'),
            'twoxtwo' => __('Two x Two'),
            'threexthree' => __('Three x Three'),
            'twoxtwowhite' => __('Two x Two White'),
            'threexthreewhite' => __('Three x Three White'),
        ];
	}
	
	public static function getDataTransition() {
		return [
            '3dcurtain-horizontal' => __('3D Curtain Horizontal'),
			'3dcurtain-vertical' => __('3D Curtain Vertical'),
			'cube-horizontal' => __('Cube Horizontal'),
			'cube' => __('Cube Vertical'),
			'curtain-1' => __('Curtain from Left'),
			'curtain-3' => __('Curtain from Middle'),
			'curtain-2' => __('Curtain from Right'),
			'fade' => __('Fade'),
			'boxfade' => __('Fade Boxes'),
			'slotfade-horizontal' => __('Fade Slots Horizontal'),
			'slotfade-vertical' => __('Fade Slots Vertical'),
			'fadetobottomfadefromtop' => __('Fade To Bottom and Fade From Top'),
			'fadetoleftfadefromright' => __('Fade To Left and Fade From Right'),
			'fadetorightfadefromleft' => __('Fade To Right and Fade From Left'),
			'fadetotopfadefrombottom' => __('Fade To Top and Fade From Bottom'),
			'fadefrombottom' => __('Fade and Slide from Bottom'),
			'fadefromleft' => __('Fade and Slide from Left'),
			'fadefromright' => __('Fade and Slide from Right'),
			'fadefromtop' => __('Fade and Slide from Top'),
			'flyin' => __('Fly In'),
			'incube-horizontal' => __('In Cube Horizontal'),
			'incube' => __('In Cube Vertical'),
			'papercut' => __('Paper Cut'),
			'parallaxtobottom' => __('Parallax to Bottom'),
			'parallaxtoleft' => __('Parallax to Left'),
			'parallaxtoright' => __('Parallax to Right'),
			'parallaxtotop' => __('Parallax to Top'),
			'random-static' => __('Random Flat'),
			'random' => __('Random Flat and Premium'),
			'random-premium' => __('Random Premium'),
			'boxslide' => __('Slide Boxes'),
			'slidehorizontal' => __('Slide Horizontal'),
			'slotslide-horizontal' => __('Slide Slots Horizontal'),
			'slotslide-vertical' => __('Slide Slots Vertical'),
			'slidedown' => __('Slide To Bottom'),
			'slideleft' => __('Slide To Left'),
			'slideright' => __('Slide To Right'),
			'slideup' => __('Slide To Top'),
			'slidevertical' => __('Slide Vertical'),
			'turnoff' => __('TurnOff Horizontal'),
			'turnoff-vertical' => __('TurnOff Vertical'),
			'scaledownfrombottom' => __('Zoom Out and Fade From Bottom'),
			'scaledownfromleft' => __('Zoom Out and Fade From Left'),
			'scaledownfromright' => __('Zoom Out and Fade From Right'),
			'scaledownfromtop' => __('Zoom Out and Fade From Top'),
			'slotzoom-horizontal' => __('Zoom Slots Horizontal'),
			'slotzoom-vertical' => __('Zoom Slots Vertical'),
			'zoomin' => __('ZoomIn'),
			'zoomout' => __('ZoomOut'),
        ];
	}
	
	public static function getSpinner() {
		return [
			'' => __('None'),
            'spinner1' => __('Spinner 1'),
            'spinner2' => __('Spinner 2'),
            'spinner3' => __('Spinner 3'),
            'spinner4' => __('Spinner 4'),
            'spinner5' => __('Spinner 5'),
        ];
	}
	
	public static function getParallaxType() {
		return [
			'off' => __('Off'),
            'mouse' => __('Mouse'),
            'scroll' => __('Scroll'),
        ];
	}
}
