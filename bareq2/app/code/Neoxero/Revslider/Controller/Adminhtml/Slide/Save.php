<?php

/**
 * Neoxero
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Neoxero.com license that is
 * available through the world-wide-web at this URL:
 * http://neoxero.com/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Neoxero
 * @package     Neoxero_Revslider
 * @copyright   Copyright (c) 2015 Neoxero (http://neoxero.com/)
 * @license     http://neoxero.com/license-agreement.html
 */

namespace Neoxero\Revslider\Controller\Adminhtml\Slide;

use Magento\Framework\App\Filesystem\DirectoryList;

/**
 * Save Slide action.
 * @category Neoxero
 * @package  Neoxero_Revslider
 * @module   Revslider
 * @author   Neoxero Developer
 */
class Save extends \Neoxero\Revslider\Controller\Adminhtml\Slide
{
    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    public function execute()
    {
        $resultRedirect = $this->_resultRedirectFactory->create();

        if ($data = $this->getRequest()->getPostValue()) {
            $model = $this->_slideFactory->create();

            if ($id = $this->getRequest()->getParam('slide_id')) {
                $model->load($id);
            }

            if (isset($_FILES['image']) && isset($_FILES['image']['name']) && strlen($_FILES['image']['name'])) {
                /*
                 * Save image upload
                 */
                try {
                    $uploader = $this->_objectManager->create(
                        'Magento\MediaStorage\Model\File\Uploader',
                        ['fileId' => 'image']
                    );
                    $uploader->setAllowedExtensions(['jpg', 'jpeg', 'gif', 'png']);

                    /** @var \Magento\Framework\Image\Adapter\AdapterInterface $imageAdapter */
                    $imageAdapter = $this->_objectManager->get('Magento\Framework\Image\AdapterFactory')->create();

                    $uploader->addValidateCallback('slide_image', $imageAdapter, 'validateUploadFile');
                    $uploader->setAllowRenameFiles(true);
                    $uploader->setFilesDispersion(true);

                    /** @var \Magento\Framework\Filesystem\Directory\Read $mediaDirectory */
                    $mediaDirectory = $this->_objectManager->get('Magento\Framework\Filesystem')
                        ->getDirectoryRead(DirectoryList::MEDIA);
                    $result = $uploader->save(
                        $mediaDirectory->getAbsolutePath(\Neoxero\Revslider\Model\Slide::BASE_MEDIA_PATH)
                    );
                    $data['image'] = \Neoxero\Revslider\Model\Slide::BASE_MEDIA_PATH.$result['file'];
                } catch (\Exception $e) {
                    if ($e->getCode() == 0) {
                        $this->messageManager->addError($e->getMessage());
                    }
                }
            } else {
                if (isset($data['image']) && isset($data['image']['value'])) {
                    if (isset($data['image']['delete'])) {
                        $data['image'] = null;
                        $data['delete_image'] = true;
                    } elseif (isset($data['image']['value'])) {
                        $data['image'] = $data['image']['value'];
                    } else {
                        $data['image'] = null;
                    }
                }
            }
			
			if (isset($_FILES['data_thumb']) && isset($_FILES['data_thumb']['name']) && strlen($_FILES['data_thumb']['name'])) {
                /*
                 * Save data_thumb upload
                 */
                try {
                    $uploader = $this->_objectManager->create(
                        'Magento\MediaStorage\Model\File\Uploader',
                        ['fileId' => 'data_thumb']
                    );
                    $uploader->setAllowedExtensions(['jpg', 'jpeg', 'gif', 'png']);

                    /** @var \Magento\Framework\Image\Adapter\AdapterInterface $imageAdapter */
                    $imageAdapter = $this->_objectManager->get('Magento\Framework\Image\AdapterFactory')->create();

                    $uploader->addValidateCallback('slide_image', $imageAdapter, 'validateUploadFile');
                    $uploader->setAllowRenameFiles(true);
                    $uploader->setFilesDispersion(true);

                    /** @var \Magento\Framework\Filesystem\Directory\Read $mediaDirectory */
                    $mediaDirectory = $this->_objectManager->get('Magento\Framework\Filesystem')
                        ->getDirectoryRead(DirectoryList::MEDIA);
                    $result = $uploader->save(
                        $mediaDirectory->getAbsolutePath(\Neoxero\Revslider\Model\Slide::BASE_MEDIA_PATH)
                    );
                    $data['data_thumb'] = \Neoxero\Revslider\Model\Slide::BASE_MEDIA_PATH.$result['file'];
                } catch (\Exception $e) {
                    if ($e->getCode() == 0) {
                        $this->messageManager->addError($e->getMessage());
                    }
                }
            } else {
                if (isset($data['data_thumb']) && isset($data['data_thumb']['value'])) {
                    if (isset($data['data_thumb']['delete'])) {
                        $data['data_thumb'] = null;
                        $data['delete_data_thumb'] = true;
                    } elseif (isset($data['data_thumb']['value'])) {
                        $data['data_thumb'] = $data['data_thumb']['value'];
                    } else {
                        $data['data_thumb'] = null;
                    }
                }
            }
			
			if (isset($_FILES['data_lazyload']) && isset($_FILES['data_lazyload']['name']) && strlen($_FILES['data_lazyload']['name'])) {
                /*
                 * Save data_lazyload upload
                 */
                try {
                    $uploader = $this->_objectManager->create(
                        'Magento\MediaStorage\Model\File\Uploader',
                        ['fileId' => 'data_lazyload']
                    );
                    $uploader->setAllowedExtensions(['jpg', 'jpeg', 'gif', 'png']);

                    /** @var \Magento\Framework\Image\Adapter\AdapterInterface $imageAdapter */
                    $imageAdapter = $this->_objectManager->get('Magento\Framework\Image\AdapterFactory')->create();

                    $uploader->addValidateCallback('slide_image', $imageAdapter, 'validateUploadFile');
                    $uploader->setAllowRenameFiles(true);
                    $uploader->setFilesDispersion(true);

                    /** @var \Magento\Framework\Filesystem\Directory\Read $mediaDirectory */
                    $mediaDirectory = $this->_objectManager->get('Magento\Framework\Filesystem')
                        ->getDirectoryRead(DirectoryList::MEDIA);
                    $result = $uploader->save(
                        $mediaDirectory->getAbsolutePath(\Neoxero\Revslider\Model\Slide::BASE_MEDIA_PATH)
                    );
                    $data['data_lazyload'] = \Neoxero\Revslider\Model\Slide::BASE_MEDIA_PATH.$result['file'];
                } catch (\Exception $e) {
                    if ($e->getCode() == 0) {
                        $this->messageManager->addError($e->getMessage());
                    }
                }
            } else {
                if (isset($data['data_lazyload']) && isset($data['data_lazyload']['value'])) {
                    if (isset($data['data_lazyload']['delete'])) {
                        $data['data_lazyload'] = null;
                        $data['delete_data_lazyload'] = true;
                    } elseif (isset($data['data_lazyload']['value'])) {
                        $data['data_lazyload'] = $data['data_lazyload']['value'];
                    } else {
                        $data['data_lazyload'] = null;
                    }
                }
            }
			
			$params = $data;		
			unset($params['form_key']);		
			unset($params['limit']);		
			unset($params['page']);		
			unset($params['in_layer']);		
			unset($params['layer_id']);		
			unset($params['layer_name']);		
			unset($params['type']);		
			unset($params['title']);		
			unset($params['layer_status']);		
			unset($params['slide_layer']);		
			$data['params'] = json_encode($params);
            $model->setData($data);

            try {
                $model->save();

                $this->messageManager->addSuccess(__('The slide has been saved.'));
                $this->_getSession()->setFormData(false);

                if ($this->getRequest()->getParam('back') === 'edit') {
                    return $resultRedirect->setPath(
                        '*/*/edit',
                        [
                            'slide_id' => $model->getId(),
                            '_current' => true,
                            'current_slider_id' => $this->getRequest()->getParam('current_slider_id'),
                            'saveandclose' => $this->getRequest()->getParam('saveandclose'),
                        ]
                    );
                } elseif ($this->getRequest()->getParam('back') === 'new') {
                    return $resultRedirect->setPath(
                        '*/*/new',
                        ['_current' => TRUE]
                    );
                }

                return $resultRedirect->setPath('*/*/');
            } catch (\Exception $e) {
                $this->messageManager->addError($e->getMessage());
                $this->messageManager->addException($e, __('Something went wrong while saving the slide.'));
            }

            $this->_getSession()->setFormData($data);

            return $resultRedirect->setPath(
                '*/*/edit',
                ['slide_id' => $this->getRequest()->getParam('slide_id')]
            );
        }

        return $resultRedirect->setPath('*/*/');
    }
}
