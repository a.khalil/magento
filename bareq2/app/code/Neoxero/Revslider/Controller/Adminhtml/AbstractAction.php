<?php

/**
 * Neoxero
 *
 * NOTICE OF LICENSE
 *
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Neoxero
 * @package     Neoxero_Revslider
 */

namespace Neoxero\Revslider\Controller\Adminhtml;

/**
 * Abstract Action
 * @category Neoxero
 * @package  Neoxero_Revslider
 * @module   Revslider
 * @author   Neoxero Developer
 */
abstract class AbstractAction extends \Magento\Backend\App\Action
{
    /**
     * @var \Magento\Backend\Helper\Js
     */
    protected $_jsHelper;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @var \Magento\Backend\Model\View\Result\ForwardFactory
     */
    protected $_resultForwardFactory;

    /**
     * @var \Magento\Framework\View\Result\LayoutFactory
     */
    protected $_resultLayoutFactory;

    /**
     * A factory that knows how to create a "page" result
     * Requires an instance of controller action in order to impose page type,
     * which is by convention is determined from the controller action class.
     *
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $_resultPageFactory;

    /**
     * @var \Magento\Backend\Model\View\Result\RedirectFactory
     */
    protected $_resultRedirectFactory;

    /**
     * Slide factory.
     *
     * @var \Neoxero\Revslider\Model\SlideFactory
     */
    protected $_slideFactory;

    /**
     * Slide Collection Factory.
     *
     * @var \Neoxero\Revslider\Model\ResourceModel\Slide\CollectionFactory
     */
    protected $_slideCollectionFactory;

	/**
     * Layer factory.
     *
     * @var \Neoxero\Revslider\Model\LayerFactory
     */
    protected $_layerFactory;

    /**
     * Layer Collection Factory.
     *
     * @var \Neoxero\Revslider\Model\ResourceModel\Layer\CollectionFactory
     */
    protected $_layerCollectionFactory;

    /**
     * Registry object.
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry;

    /**
     * File Factory.
     *
     * @var \Magento\Framework\App\Response\Http\FileFactory
     */
    protected $_fileFactory;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Neoxero\Revslider\Model\SlideFactory $slideFactory
     * @param \Neoxero\Revslider\Model\SliderFactory $sliderFactory
     * @param \Neoxero\Revslider\Model\ResourceModel\Slide\CollectionFactory $slideCollectionFactory
     * @param \Neoxero\Revslider\Model\ResourceModel\Slider\CollectionFactory $sliderCollectionFactory
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Magento\Framework\App\Response\Http\FileFactory $fileFactory
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param \Magento\Framework\View\Result\LayoutFactory $resultLayoutFactory
     * @param \Magento\Backend\Model\View\Result\ForwardFactory $resultForwardFactory
     * @param \Magento\Backend\Model\View\Result\RedirectFactory $resultRedirectFactory
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Backend\Helper\Js $jsHelper
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Neoxero\Revslider\Model\SlideFactory $slideFactory,
        \Neoxero\Revslider\Model\LayerFactory $layerFactory,
        \Neoxero\Revslider\Model\SliderFactory $sliderFactory,
        \Neoxero\Revslider\Model\ResourceModel\Slide\CollectionFactory $slideCollectionFactory,
        \Neoxero\Revslider\Model\ResourceModel\Layer\CollectionFactory $layerCollectionFactory,
        \Neoxero\Revslider\Model\ResourceModel\Slider\CollectionFactory $sliderCollectionFactory,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Framework\App\Response\Http\FileFactory $fileFactory,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\View\Result\LayoutFactory $resultLayoutFactory,
        \Magento\Backend\Model\View\Result\ForwardFactory $resultForwardFactory,
        \Magento\Backend\Model\View\Result\RedirectFactory $resultRedirectFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Backend\Helper\Js $jsHelper
    ) {
      // print_r(get_class_methods($this));
      // print_r(get_class_methods($context));


        $this->_coreRegistry = $coreRegistry;
        $this->_fileFactory = $fileFactory;
         $this->_storeManager =   $storeManager ;// $context-> getStoreManager () ;
        //$this->_storeManager =   $context-> getStoreManager () ;
        $this->_jsHelper = $jsHelper;

        $this->_resultPageFactory = $resultPageFactory;
        $this->_resultLayoutFactory = $resultLayoutFactory;
        $this->_resultForwardFactory = $resultForwardFactory;
        $this->_resultRedirectFactory = $resultRedirectFactory;
        //$this->_resultRedirectFactory = $context-> getResultRedirectFactory ();//getRedirectFactory();

        $this->_slideFactory = $slideFactory;
        $this->_layerFactory = $layerFactory;
        $this->_sliderFactory = $sliderFactory;
        $this->_slideCollectionFactory = $slideCollectionFactory;
        $this->_layerCollectionFactory = $layerCollectionFactory;
        $this->_sliderCollectionFactory = $sliderCollectionFactory;

        parent::__construct($context);
        
    }
}
