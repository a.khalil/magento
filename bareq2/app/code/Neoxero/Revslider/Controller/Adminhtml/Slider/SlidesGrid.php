<?php

/**
 * Neoxero
 *
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Neoxero
 * @package     Neoxero_Revslider
 */

namespace Neoxero\Revslider\Controller\Adminhtml\Slider;

/**
 * Slides Grid action
 * @category Neoxero
 * @package  Neoxero_Revslider
 * @module   Revslider
 * @author   Neoxero Developer
 */
class SlidesGrid extends \Neoxero\Revslider\Controller\Adminhtml\Slider
{
    public function execute()
    {
        $resultLayout = $this->_resultLayoutFactory->create();
        $resultLayout->getLayout()->getBlock('revslider.slider.edit.tab.slides')
                     ->setInSlide($this->getRequest()->getPost('slide', null));

        return $resultLayout;
    }
}
