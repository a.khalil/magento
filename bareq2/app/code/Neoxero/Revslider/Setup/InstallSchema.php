<?php

/**
 * Neoxero
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Neoxero.com license that is
 * available through the world-wide-web at this URL:
 * http://neoxero.com/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Neoxero
 * @package     Neoxero_Revslider
 * @copyright   Copyright (c) 2015 Neoxero (http://neoxero.com/)
 * @license     http://neoxero.com/license-agreement.html
 */

namespace Neoxero\Revslider\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * Install schema
 * @category Neoxero
 * @package  Neoxero_Revslider
 * @module   Revslider
 * @author   Neoxero Developer
 */
class InstallSchema implements InstallSchemaInterface
{
    /**
     * {@inheritdoc}
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;

        $installer->startSetup();

        /*
         * Drop tables if exists
         */
        $installer->getConnection()->dropTable($installer->getTable('revslider_slider'));
        $installer->getConnection()->dropTable($installer->getTable('revslider_slide'));
        $installer->getConnection()->dropTable($installer->getTable('revslider_layer'));

        /*
         * Create table Neoxero_revslider_slider
         */
        $table = $installer->getConnection()->newTable(
            $installer->getTable('revslider_slider')
        )->addColumn(
            'slider_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            10,
            ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
            'Slider ID'
        )->addColumn(
            'title',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            ['nullable' => false, 'default' => ''],
            'Slider title'
        )->addColumn(
            'status',
            \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
            null,
            ['nullable' => false, 'default' => '1'],
            'Slider status'
        )->addColumn(
            'params',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            null,
            ['nullable' => true],
            'Slider params data'
        )->addColumn(
            'custom_style',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            null,
            ['nullable' => true],
            'Slider custom CSS'
        )->addIndex(
            $installer->getIdxName('revslider_slider', ['slider_id']),
            ['slider_id']
        )->addIndex(
            $installer->getIdxName('Neoxero_revslider_slider', ['status']),
            ['status']
        );
        $installer->getConnection()->createTable($table);
        /*
         * End create table Neoxero_revslider_slider
         */

        /*
         * Create table Neoxero_revslider_slide
         */
        $table = $installer->getConnection()->newTable(
            $installer->getTable('revslider_slide')
        )->addColumn(
            'slide_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            10,
            ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
            'Slide ID'
        )->addColumn(
            'name',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            ['nullable' => false, 'default' => ''],
            'Slide name'
        )->addColumn(
            'order_slide',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            10,
            ['nullable' => true, 'default' => 0],
            'Slide order'
        )->addColumn(
            'slider_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            10,
            ['nullable' => true],
            'Slider Id'
        )->addColumn(
            'image',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            ['nullable' => true],
            'Slide image'
        )->addColumn(
            'image_alt',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            ['nullable' => true],
            'Slide image alt'
        )->addColumn(
            'data_thumb',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            ['nullable' => true],
            'Slide thumbnail'
        )->addColumn(
            'status',
            \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
            null,
            ['nullable' => false, 'default' => '1'],
            'Slide status'
        )->addColumn(
            'params',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            null,
            ['nullable' => true],
            'Slide params data'
        )->addColumn(
            'style_slide',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            ['nullable' => true],
            'Slide style'
        )->addIndex(
            $installer->getIdxName('Neoxero_revslider_slide', ['slide_id']),
            ['slide_id']
        )->addIndex(
            $installer->getIdxName('Neoxero_revslider_slide', ['slider_id']),
            ['slider_id']
        )->addIndex(
            $installer->getIdxName('Neoxero_revslider_slide', ['status']),
            ['status']
        );
        $installer->getConnection()->createTable($table);
        /*
         * End create table Neoxero_revslider_slide
         */
		 
		/*
         * Create table Neoxero_revslider_layer
         */
        $table = $installer->getConnection()->newTable(
            $installer->getTable('revslider_layer')
        )->addColumn(
            'layer_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            10,
            ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
            'Layer ID'
        )->addColumn(
            'name',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            ['nullable' => false, 'default' => ''],
            'layer name'
        )->addColumn(
            'type',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            10,
            ['nullable' => true, 'default' => 0],
            'Layer type'
        )
		->addColumn(
            'order_layer',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            10,
            ['nullable' => true, 'default' => 0],
            'Layer order'
        )->addColumn(
            'slide_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            10,
            ['nullable' => true],
            'Slide Id'
        )->addColumn(
            'status',
            \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
            null,
            ['nullable' => false, 'default' => '1'],
            'Layer status'
        )->addColumn(
            'params',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            null,
            ['nullable' => true],
            'Layer params data'
        )->addColumn(
            'captions',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            null,
            ['nullable' => true],
            'Layer captions data'
        )->addIndex(
            $installer->getIdxName('revslider_layer', ['layer_id']),
            ['layer_id']
        )->addIndex(
            $installer->getIdxName('revslider_layer', ['slide_id']),
            ['slide_id']
        )->addIndex(
            $installer->getIdxName('revslider_layer', ['status']),
            ['status']
        );
        $installer->getConnection()->createTable($table);
        /*
         * End create table Neoxero_revslider_layer
         */

        $installer->endSetup();
    }
}
