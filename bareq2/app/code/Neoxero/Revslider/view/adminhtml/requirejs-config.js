var config = {
	"map": {
		"*": {
			"Neoxero/plugins": "Neoxero_Revslider/rs-plugin/js/jquery.themepunch.tools.min",
			"Neoxero/revolution": "Neoxero_Revslider/rs-plugin/js/jquery.themepunch.revolution.min"
		}
	},
	"paths": {            
		"Neoxero/plugins": "Neoxero_Revslider/rs-plugin/js/jquery.themepunch.tools.min",
		"Neoxero/revolution": "Neoxero_Revslider/rs-plugin/js/jquery.themepunch.revolution.min"
	},
	"shim": {
		"Neoxero_Revslider/rs-plugin/js/jquery.themepunch.tools.min": ["jquery"],
		"Neoxero_Revslider/rs-plugin/js/jquery.themepunch.revolution.min": ["jquery"]
	}
};
