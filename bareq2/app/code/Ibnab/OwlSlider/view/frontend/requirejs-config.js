var config = {
    // map: {
    //     '*': {
    //       ibnabowlsliders:       'Ibnab_OwlSlider/js/owl.carousel.min',
    //     }
    // }
    paths: {
        'ibnabowlsliders': "Magento_Catalog/js/owlcarousel"
    },
    shim: {
        'ibnabowlsliders': {
            deps: ['jquery']
        }
    }
}
