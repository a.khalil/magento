/**
 * Copyright © 2016 Neoxero. All rights reserved.
 * See COPYING.txt for license details.
 */

var config = {
    map: {
        '*': {
            priceBox:               'Magento_Catalog/js/price-box',
            priceOptionDate:        'Magento_Catalog/js/price-option-date',
            priceOptionFile:        'Magento_Catalog/js/price-option-file',
            priceOptions:           'Magento_Catalog/js/price-options',
            priceUtils:             'Magento_Catalog/js/price-utils'
        }
    } ,
        paths: {
            bootstraper: 'js/bootstraper',
            nx_luma: 'js/nx_luma',
        },

    shim: {
        'bootstraper': ['jquery'],
        'nx_luma': ['jquery'],

    }
};
